package com.happytap.mustache.interfaces;

import com.happytap.mustache.domain.Photo;

public interface OnPhotoPicked {

	void onPhoto(Photo photo);
	
}
