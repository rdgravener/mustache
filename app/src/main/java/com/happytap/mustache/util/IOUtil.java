package com.happytap.mustache.util;

import android.content.Context;
import android.os.Build;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by gravener on 9/12/14.
 */
public class IOUtil {

    public static void copy(Context ctx, InputStream in, String fileOut) {
        try {
            FileOutputStream out = ctx.openFileOutput(fileOut, Context.MODE_PRIVATE);
            byte[] buff = new byte[1024];
            int read = 0;
            try {
                while ((read = in.read(buff)) > 0) {
                    out.write(buff, 0, read);
                }
            } finally {
                in.close();

                out.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static File getCacheDir(Context ctx) {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT) {
            return ctx.getExternalCacheDir();
        } else {
            return ctx.getCacheDir();
        }
    }

}
