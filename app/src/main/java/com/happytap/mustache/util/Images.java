package com.happytap.mustache.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;

public class Images {

	public static File resizeImage(Context ctx, int maxWidth, int maxHeight,
			int quality, String prefix, Uri uri) {
		File file = new File(getAlbumDir(ctx),uri.getLastPathSegment()+"_" + maxWidth+"x"+maxHeight+".jpg"); 
				
		if (file.exists()) {
			return file;
		}
		file.getParentFile().mkdirs();
		Bitmap b = loadImage(ctx, maxWidth, maxHeight, uri);

		if (b != null) {

			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(file);
				int newW,newH;
				if(b.getWidth()>b.getHeight()) {
					newW = maxWidth;
					newH = (int) (maxWidth/(float) b.getWidth() * b.getHeight());
				} else {
					newH = maxHeight;
					newW = (int) (maxHeight/(float) b.getHeight() * b.getWidth());
				}
				b = Bitmap.createScaledBitmap(b, newW, newH, false);
				b.compress(CompressFormat.JPEG, quality, fos);
			} catch (Exception e) {
				file = null;
			} finally {
				b.recycle();
			}

			return file;
		}

		return null;
	}	
	
	public static String bytesToHex(byte[] bytes) {
	    final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
	        v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	/**
	 * 
	 * @param ctx
	 * @param uri
	 * @param file
	 * @return SHA hash.
	 */
	public static String copy(Context ctx, Uri uri, File file) {
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			if (uri.getScheme().equals("file")) {
				is = new FileInputStream(uri.getPath());
			} else {
				is = ctx.getContentResolver().openInputStream(uri);
			}
			fos = new FileOutputStream(file);
			byte[] buf = new byte[1024 * 5];
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			int read;
			while ((read = is.read(buf)) != -1) {
				fos.write(buf, 0, read);
				sha.update(buf, 0, read);
			}
			return bytesToHex(sha.digest());
		} catch (Exception e) {
			return null;
		} finally {
			try {
				is.close();
			} catch (Exception e) {}
			try {
				fos.close();
			} catch (Exception e) {
				
			}
		}

	}

	public static BitmapFactory.Options getImageSize(Context ctx, Uri uri) {
		if (uri.getScheme().equals("file")) {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			String path = uri.getPath();
			BitmapFactory.decodeFile(path, o);
			return o;
		} else {
			InputStream is = null;
			try {
				is = ctx.getContentResolver().openInputStream(uri);
			} catch (FileNotFoundException e) {
				return null;
			}
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, o);
			try {
				is.close();
				// is = ctx.getContentResolver().openInputStream(uri);
			} catch (Exception e) {
				return null;
			}
			return o;
		}
	}
	
	public static File getAlbumDir(Context ctx) {
		File file = new File(Environment.getExternalStorageDirectory(),
				"Android/data/" + ctx.getPackageName() + "/files/");
		String state = Environment.getExternalStorageState();
		System.out.println(state);
		if (!file.exists()) {
			System.out.println(file);
			System.out.println(file.mkdirs());
		}
		return file;
	}

	public static Bitmap loadImage(Context ctx, int maxWidth, int maxHeight,
			Uri uri) {
		if (uri.getScheme().equals("file")) {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			String path = uri.getPath();
			BitmapFactory.decodeFile(path, o);
			int scale = 0;
			int newWidth = o.outWidth;
			int newHeight = o.outHeight;
			while (newWidth > maxWidth || newHeight > maxHeight) {
				scale += 1;
				newWidth = newWidth / 2;
				newHeight = newHeight / 2;
			}
			o.inJustDecodeBounds = false;
			o.inSampleSize = scale;
			final Bitmap b = BitmapFactory.decodeFile(path, o);
			return b;
		}
		if (uri.getScheme().equals("content")) {
			InputStream is = null;
			try {
				is = ctx.getContentResolver().openInputStream(uri);
			} catch (FileNotFoundException e) {
				return null;
			}
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, o);
			try {
				is.close();
				is = ctx.getContentResolver().openInputStream(uri);
			} catch (Exception e) {
				return null;
			}
			int scale = 0;
			int newWidth = o.outWidth;
			int newHeight = o.outHeight;
			while (newWidth > maxWidth || newHeight > maxHeight) {
				scale += 1;
				newWidth = newWidth / 2;
				newHeight = newHeight / 2;
			}
			o.inJustDecodeBounds = false;
			o.inSampleSize = scale;
			final Bitmap b = BitmapFactory.decodeStream(is, null, o);
			return b;
		}
		return null;
	}

}
