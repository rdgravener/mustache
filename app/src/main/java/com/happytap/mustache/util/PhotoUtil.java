package com.happytap.mustache.util;

import android.content.Context;
import android.content.res.Resources;

import android.media.ExifInterface;
import android.util.Log;

import com.happytap.mustache.domain.FaceSimple;
import com.happytap.mustache.R;
import com.happytap.mustache.domain.Media;
import com.happytap.mustache.domain.PointF;

import org.bytedeco.javacpp.flandmark;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_objdetect;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.bytedeco.javacpp.BytePointer;

import static org.bytedeco.javacpp.flandmark.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_highgui.*;
/**
 * Created by gravener on 9/13/14.
 */
public class PhotoUtil {

    public static List<FaceSimple> mustache(Context ctx, File file) {
        return mustache(ctx, new Media(file,null));
    }

    public static List<FaceSimple> mustache(Context ctx, Media media) {
        Resources r = ctx.getResources();
        InputStream faceIn = r.openRawResource(R.raw.haarcascade_frontalface_alt);
        InputStream mouthIn = r.openRawResource(R.raw.haarcascade_mouth);
        InputStream flandmarkIn = r.openRawResource(R.raw.flandmark_model);
        File face = ctx.getFileStreamPath("haar_face.xml");
        File flandmark_model = ctx.getFileStreamPath("flandmark_model.dat");
        //File mouth = getApplication().getFileStreamPath("haar_mouth.xml");
        IOUtil.copy(ctx, faceIn, face.getName());
        IOUtil.copy(ctx, flandmarkIn, flandmark_model.getName());
        //IOUtil.copy(getApplication(),mouthIn,mouth.getName());
        final flandmark.FLANDMARK_Model model = flandmark_init(flandmark_model.toString());
        Log.d("INSTANT", "getting bitmap");
        System.out.println(media.getFile().getAbsolutePath());
        opencv_core.IplImage image = cvLoadImage(media.getFile().getAbsolutePath());
        opencv_core.CvMemStorage storage = opencv_core.CvMemStorage.create();
        opencv_objdetect.CvHaarClassifierCascade cascade = null;
        //opencv_objdetect.CvHaarClassifierCascade mouthCascade = null;
        try {
            cascade = new opencv_objdetect.CvHaarClassifierCascade(cvLoad(face.toString()));
            //mouthCascade = new opencv_objdetect.CvHaarClassifierCascade(cvLoad(mouth.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("image is " + image);
        final opencv_core.IplImage imageBW = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
        cvCvtColor(image, imageBW, CV_BGR2GRAY);
        final int[] bbox = new int[4];
        final double[] landmarks = new double[2 * model.data().options().M()];
        try {
            List<FaceSimple> faces = detectFaceInImage(image, imageBW, cascade, model, bbox, landmarks,new File("/sdcard/output.jpg"));

            return faces;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static List<FaceSimple> detectFaceInImage(final IplImage orig,
                                         final IplImage input,
                                         final opencv_objdetect.CvHaarClassifierCascade cascade,
                                         final FLANDMARK_Model model,
                                         final int[] bbox,
                                         final double[] landmarks,
                                         final File destination) throws Exception {

        CvMemStorage storage = cvCreateMemStorage(0);
        cvClearMemStorage(storage);
        try {
            double search_scale_factor = 1.1;
            int flags = opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;
            CvSize minFeatureSize = cvSize(40, 40);
            CvSeq rects = opencv_objdetect.cvHaarDetectObjects(input, cascade, storage, search_scale_factor, 2, flags, minFeatureSize, cvSize(0, 0));
            int nFaces = rects.total();
            List<FaceSimple> faces = new ArrayList<FaceSimple>(nFaces);
            if (nFaces == 0) {
                return faces;
            }

            for (int iface = 0; iface < nFaces; ++iface) {
                BytePointer elem = cvGetSeqElem(rects, iface);
                CvRect rect = new CvRect(elem);

                bbox[0] = rect.x();
                bbox[1] = rect.y();
                bbox[2] = rect.x() + rect.width();
                bbox[3] = rect.y() + rect.height();

                flandmark_detect(input, bbox, model, landmarks);

                // display landmarks
                cvRectangle(orig, cvPoint(bbox[0], bbox[1]), cvPoint(bbox[2], bbox[3]), CV_RGB(255, 0, 0));
                cvRectangle(orig,
                        cvPoint((int) model.bb().get(0), (int) model.bb().get(1)),
                        cvPoint((int) model.bb().get(2), (int) model.bb().get(3)), CV_RGB(0, 0, 255));
                cvCircle(orig,
                        cvPoint((int) landmarks[0], (int) landmarks[1]), 3, CV_RGB(0, 0, 255), CV_FILLED, 8, 0);

                CvScalar red = CV_RGB(255,0,0);
                CvScalar green = CV_RGB(0,255,0);
                CvScalar yellow = CV_RGB(255,255,0);
                CvScalar orange = CV_RGB(255,165,0);
                CvScalar blue = CV_RGB(0,0,255);
                CvScalar pink = CV_RGB(255,20,147);
                CvScalar gray = CV_RGB(128,128,128);
                List<CvScalar> colors = java.util.Arrays.asList(new CvScalar[]{red,green,yellow,orange,blue,pink,gray});
                Iterator<CvScalar> colorIter = colors.iterator();
                PointF mouthLeft = null,mouthRight=null;
                System.out.println("");
                for (int i = 2; i < 2 * model.data().options().M(); i += 2) {
                    CvScalar color = colorIter.next();
                    float x = (float)landmarks[i];
                    float y = (float)landmarks[i+1];
                    System.out.println(x+ "," + y);
                    if(color==yellow) {
                       mouthLeft = new PointF(x,y);
                       System.out.println("left is " + mouthLeft);
                    }
                    if (color == orange) {
                        mouthRight = new PointF(x, y);
                        System.out.println("right is " + mouthLeft);
                    }
                    cvCircle(orig, cvPoint((int) x, (int) y), 3, color, CV_FILLED, 8, 0);
                }

                //PointF mouthRight = new PointF((float)landmarks[2+4*2],(float)landmarks[2+4*2+1]);
                PointF nose = new PointF((float)landmarks[2+6*2],(float)landmarks[2+6*2+1]);
                PointF eyeLeftLeft = new PointF((float)landmarks[2+5*2],(float)landmarks[2+5*2+1]);
                PointF eyeLeftRight = new PointF((float)landmarks[2+1*2],(float)landmarks[2+1*2+1]);
                PointF eyeRightLeft = new PointF((float)landmarks[2+2*2],(float)landmarks[2+2*2+1]);
                PointF eyeRightRight = new PointF((float)landmarks[2+6*2],(float)landmarks[2+6*2+1]);

                FaceSimple face = new FaceSimple();
                face.setEyeLeftLeft(eyeLeftLeft).setEyeLeftRight(eyeLeftRight)
                .setEyeRightLeft(eyeRightLeft).setEyeRightRight(eyeRightRight)
                .setNose(nose).setMouthLeft(mouthLeft).setMouthRight(mouthRight);
                faces.add(face);
            }
            if(destination!=null) {
                cvSaveImage(destination.getAbsolutePath(), orig);
                //cvSmooth(orig,orig,CV_GAUSSIAN,91,91,40,0);
                //cvSmooth(orig,orig,CV_GAUSSIAN,91,91,40,0);
                //cvSaveImage(destination.getAbsolutePath()+".g", orig);
            }
            return faces;
        } finally {
            cvReleaseMemStorage(storage);
        }
    }


}
