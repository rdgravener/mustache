package com.happytap.mustache.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

public class FragmentPaywallAdapter extends BaseAdapter {

	private static final String[] SKUS = new String[]{"remove.ads","remove.branding"};
	private static final String[] NAMES = new String[]{"Remove ads", "Remove branding from shared photos"};
	
	
	
	@Override
	public int getCount() {
		return SKUS.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CheckedTextView t = new CheckedTextView(parent.getContext());
		String name = NAMES[position];
		t.setText(name);
		return t;
	}

}
