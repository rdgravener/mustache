package com.happytap.mustache.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final int VERSION = 1;
	
	private static DBHelper instance;

	public DBHelper(Context context, String name) {
		super(context, name, null, VERSION);
		instance = this;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createTables(0,VERSION, db);
	}

	private void createTables(int from, int to, SQLiteDatabase db) {
		if(from==0) {
			createPhotos(db);
		}
		
	}

	private void createPhotos(SQLiteDatabase db) {
		db.execSQL("create table photos(id varchar(50) unique, created integer, file_name text, resized_file_name text, resized_height integer, resized_width integer, hash varchar(255), url varchar(255), width integer, height integer, faces_checked integer, thumbnail text, thumbnail_width integer, thumbnail_height integer)");
		db.execSQL("create table mustache_photos(id varchar(50) unique, file_name text, resized_file_name unique, resized_height integer, resized_width integer, hash varchar(255), url varchar(255), width integer, height integer, faces_checked integer)");
		db.execSQL("create table faces(position integer, photo_id varchar(50), eye_left_x real, eye_right_x real, eye_left_y real, eye_right_y real, nose_x real, nose_y real, mouth_left_x real, mouth_left_y real,  mouth_right_y real, mouth_right_x real, confidence real, age real, mouth_open_wide real, glasses real, smile real, eyes_closed real, sex real)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		createTables(oldVersion,newVersion,db);
	}
	
	public static DBHelper getInstance() {
		return instance;
	}

}
