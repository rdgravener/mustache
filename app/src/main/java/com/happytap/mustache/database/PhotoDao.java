package com.happytap.mustache.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.happytap.mustache.domain.Face;
import com.happytap.mustache.domain.FaceResponse;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.domain.PointF;

public class PhotoDao {

	private static PhotoDao instance;

	public void savePhoto(Photo photo) {
		ContentValues cv = new ContentValues(3);
		cv.put("file_name", photo.fileName);
		cv.put("id", photo.id);
		if (photo.hash != null) {
			cv.put("hash", photo.hash);
		}
		if (photo.url != null) {
			cv.put("url", photo.url);
		}
		if(photo.width!=0) {
			cv.put("width", photo.width);
		}
		if(photo.height!=0) {
			cv.put("height", photo.height);
		}
		cv.put("created", photo.created);
		if(photo.resizedFileName!=null) {
			cv.put("resized_file_name", photo.resizedFileName);
		}
		cv.put("resized_width", photo.resizedWidth);
		cv.put("resized_height", photo.resizedHeight);
		cv.put("faces_checked", photo.facesChecked ? 1 : 0);
		cv.put("thumbnail", photo.thumbnail);
		cv.put("thumbnail_width", photo.thumbnailWidth);
		cv.put("thumbnail_height", photo.thumbnailHeight);
		DBHelper.getInstance()
				.getWritableDatabase()
				.insertWithOnConflict("photos", null, cv,
						SQLiteDatabase.CONFLICT_REPLACE);
	}
	
	public List<Face> getFaces(String photoId) {
		Cursor c = DBHelper.getInstance().getReadableDatabase().rawQuery("select * from faces where photo_id=?", new String[] {photoId});
		List<Face> faces = new ArrayList<Face>();
		while(c.moveToNext()) {
			Face face = fromFace(c);
			faces.add(face);
		}
		return faces;
	}
	
	public void saveFace(String photoId, FaceResponse face) {
		ContentValues cv = new ContentValues();
		cv.put("photo_id", photoId);
		for(int i = 0; i < face.getFaces().size(); i++) {
			cv.put("position",i);
			Face f = face.getFaces().get(i);
			if(f.getEyeLeft()!=null) {
				cv.put("eye_left_x", f.getEyeLeft().x);
				cv.put("eye_left_y", f.getEyeLeft().y);
			}
			if(f.getEyeRight()!=null) {
				cv.put("eye_right_x", f.getEyeRight().x);
				cv.put("eye_right_y", f.getEyeRight().y);
			}
			if(f.getMouthLeft()!=null) {
				cv.put("mouth_left_x", f.getMouthLeft().x);
				cv.put("mouth_left_y", f.getMouthLeft().y);
			}
			if(f.getMouthRight()!=null) {
				cv.put("mouth_right_x", f.getMouthRight().x);
				cv.put("mouth_right_y", f.getMouthRight().y);
			}
			if(f.getNose()!=null) {
				cv.put("nose_x", f.getNose().x);
				cv.put("nose_y", f.getNose().y);
			}
			cv.put("sex", f.sex);
			cv.put("mouth_open_wide", f.mouthOpenWide);
			cv.put("confidence", f.confidence);
			cv.put("age", f.age);
			cv.put("glasses", f.glasses);					
			DBHelper.getInstance().getWritableDatabase().insert("faces", null, cv);			
		}
	}
	
	public Photo getLatest() {
		Cursor c = DBHelper.getInstance().getWritableDatabase().rawQuery("select * from photo limit 1", null);
		Photo photo = null;
		if(c.moveToNext()) {
			photo = from(c);
		}		
		c.close();
		return photo;
	}
	
	private Face fromFace(Cursor c) {
		Face face = new Face();
		int eyeLeftX = c.getColumnIndex("eye_left_x");
		int eyeLeftY = c.getColumnIndex("eye_left_y");
		int eyeRightX = c.getColumnIndex("eye_right_x");
		int eyeRightY = c.getColumnIndex("eye_right_y");
		int noseX = c.getColumnIndex("nose_x");
		int noseY = c.getColumnIndex("nose_y");
		int mouthLeftX = c.getColumnIndex("mouth_left_x");
		int mouthLeftY = c.getColumnIndex("mouth_left_y");
		int mouthRightX = c.getColumnIndex("mouth_right_x");
		int mouthRightY = c.getColumnIndex("mouth_right_y");
		int sex = c.getColumnIndex("sex");
		int mouthWideOpen = c.getColumnIndex("mouth_open_wide");
		int eyesClosed = c.getColumnIndex("eyes_closed");
		int confidence = c.getColumnIndex("confidence");
		int age = c.getColumnIndex("age");
		int glasses = c.getColumnIndex("glasses");
		
		if(!c.isNull(c.getColumnIndex("eye_left_x"))) {
			face.eyeLeft = new PointF(c.getFloat(eyeLeftX), c.getFloat(eyeLeftY));		
		}
		if(!c.isNull(eyeLeftX)) {
			face.eyeRight = new PointF(c.getFloat(eyeRightX), c.getFloat(eyeRightY));		
		}
		if(!c.isNull(eyeRightX)) {
			face.nose = new PointF(c.getFloat(noseX), c.getFloat(noseY));		
		}
		if(!c.isNull(noseX)) {
			face.nose = new PointF(c.getFloat(noseX), c.getFloat(noseY));		
		}
		if(!c.isNull(mouthLeftX)) {
			face.mouthLeft = new PointF(c.getFloat(mouthLeftX), c.getFloat(mouthLeftY));		
		}
		if(!c.isNull(mouthRightX)) {
			face.mouthRight = new PointF(c.getFloat(mouthRightX), c.getFloat(mouthRightY));
		}
		face.sex = c.getFloat(sex);
		face.mouthOpenWide = c.getFloat(mouthWideOpen);
		face.age = c.getFloat(age);
		face.glasses = c.getFloat(glasses);
		face.eyeClosed = c.getFloat(eyesClosed);
		face.confidence = c.getFloat(confidence);
		return face;
	}

	private Photo from(Cursor c) {
		Photo photo = new Photo();
		photo.fileName = c.getString(c.getColumnIndex("file_name"));
		photo.hash = c.getString(c.getColumnIndex("hash"));
		photo.url = c.getString(c.getColumnIndex("url"));
		photo.created = c.getLong(c.getColumnIndex("created"));
		photo.resizedFileName = c.getString(c.getColumnIndex("resized_file_name"));
		photo.width = c.getInt(c.getColumnIndex("width"));
		photo.height = c.getInt(c.getColumnIndex("height"));
		photo.id = c.getString(c.getColumnIndex("id"));
		photo.facesChecked = c.getInt(c.getColumnIndex("faces_checked"))!=0;
		photo.resizedWidth = c.getInt(c.getColumnIndex("resized_width"));
		photo.resizedHeight = c.getInt(c.getColumnIndex("resized_height"));
		photo.thumbnail = c.getString(c.getColumnIndex("thumbnail"));
		photo.thumbnailWidth = c.getInt(c.getColumnIndex("thumbnail_width"));
		photo.thumbnailHeight = c.getInt(c.getColumnIndex("thumbnail_height")); 
		return photo;
	}
	
	public void updateThumbnail(String id, String uri, int width, int height) {
		ContentValues cv = new ContentValues();
		cv.put("thumbnail", uri);
		cv.put("thumbnail_width", width);
		cv.put("thumbnail_height", height);
		DBHelper.getInstance().getWritableDatabase().update("photos",cv , "id=?", new String[]{id});
	}
	
	public static PhotoDao getInstance() {
		if (instance == null) {
			instance = new PhotoDao();
		}
		return instance;
	}

	public Photo getPhoto(String photoID) {
		Cursor c = DBHelper.getInstance().getReadableDatabase().rawQuery("select * from photos where id = ?", new String[] {photoID});
		Photo photo = null;
		if(c.moveToNext()) {
			photo = from(c);
		}
		c.close();
		return photo;
	}
}
