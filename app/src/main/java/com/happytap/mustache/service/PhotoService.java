package com.happytap.mustache.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.happytap.mustache.domain.FaceSimple;
import com.happytap.mustache.domain.Media;
import com.happytap.mustache.observer.PhotoObserver;
import com.happytap.mustache.util.PhotoUtil;

import java.util.List;

/**
 * Created by gravener on 9/13/14.
 */
public class PhotoService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    PhotoObserver photosObserver;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    Handler handler = new Handler();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("PhotoService", "onStartCommand " + flags + " " + startId);
        if(photosObserver==null) {
            photosObserver = new PhotoObserver(this) {
                @Override
                public void onChange(boolean selfChange) {
                    Log.d("PhotoService", "photosObserver.onChange");
                    super.onChange(selfChange);
                    Media media = readFromMediaStore(ctx, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    final List<FaceSimple> faces = PhotoUtil.mustache(ctx, media);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx,"There is " + faces.size() + " faces", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            };
            this.getApplicationContext()
                    .getContentResolver()
                    .registerContentObserver(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false,
                            photosObserver);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        getApplicationContext().getContentResolver().unregisterContentObserver(photosObserver);
    }
}
