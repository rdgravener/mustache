package com.happytap.mustache.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.happytap.mustache.R;

public class MovieCountdown extends View {

	private int milliseconds;
	private double angle;
	
	private Paint paint;
	
	public MovieCountdown(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MovieCountdown(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MovieCountdown(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	private Path path = new Path();
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(paint==null) {
			paint = new Paint();
			paint.setColor(Color.BLACK);
			paint.setStyle(Style.STROKE);
			paint.setFlags(Paint.ANTI_ALIAS_FLAG);
			paint.setStrokeWidth(getResources().getDimension(R.dimen.stroke_width));
		}
		
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		
		int maxDiameter = Math.min(width,height);
		
		int padding = (int) Math.round(maxDiameter*.05);
		
		int radius = ((maxDiameter-padding) / 2);
		
		int centerPointX = width/2;
		int centerPointY = height/2;				
		
		path.moveTo(width/2, height/2);
		
		path.moveTo(width/2, 0);
		
		
		
		canvas.drawLine(0, height/2, width, height/2, paint);
		canvas.drawLine(width/2, 0, width/2, height, paint);
		
		canvas.drawCircle(centerPointX, centerPointY, radius, paint);
		
		radius*=.9;
		
		canvas.drawCircle(centerPointX, centerPointY, radius, paint);
		
		
		
	}

}
