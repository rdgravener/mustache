package com.happytap.mustache.views;

import java.io.File;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.concurrent.Future;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.happytap.mustache.R;
import com.happytap.mustache.database.DBHelper;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.fragments.FragmentLeftMenuAdapter;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.ThreadHelper;

public class MenuView extends RelativeLayout {

	public static interface MenuItemClicked {
		
		void onMustache(Photo photo);
		
		void onOther(Object o);
		
		void onNav(int id);
		
	}
	
	private ListView list;
	
	private MenuItemClicked onMenuItemClicked;
	
	private FragmentLeftMenuAdapter adapter;
	
	public void setOnMenuItemClicked(MenuItemClicked onMenuItemClicked) {
		this.onMenuItemClicked = onMenuItemClicked;
	}

	public MenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.menu, this);
		list = (ListView) findViewById(R.id.list);

		list.setAdapter(adapter = new FragmentLeftMenuAdapter(context));
		//list.setAdapter(new MenuAdapter(getContext()));
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Object tag = arg1.getTag();
				
				//MenuItemView v = (MenuItemView) arg1;
				//Photo photo = PhotoDao.getInstance().getPhoto(v.getPhotoID());
				if(onMenuItemClicked!=null) {
					//onMenuItemClicked.onMustache(photo);
					if(tag instanceof Integer) {
						onMenuItemClicked.onNav((Integer)tag);
						adapter.setSelectedDrawable((Integer)tag);
					}
				}
			}
		});
	}

	public static class MenuAdapter extends CursorAdapter {

		private static final DateFormat FORMATTER = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
		public MenuAdapter(Context ctx) {
			super(ctx, null, false);
			changeCursor(DBHelper.getInstance().getReadableDatabase()
					.rawQuery("select id as _id, file_name, created, thumbnail from photos", null));
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void bindView(View v, final Context arg1, Cursor c) {
			// TODO Auto-generated method stub
			MenuItemView view = (MenuItemView) v;
			final String fileName = getFileName(c);
			String created = getCreated(c);
			String id = getId(c);
			String thumb = getThumbnail(c);
			view.update(id, fileName, thumb, created);
			if(thumb==null) {
				final ViewHolder h = (ViewHolder)view.getTag();
				if(h.thumb!=null) {
					h.thumb.cancel(true);
				}
				h.id = id;
				h.fileName = fileName;
				h.thumb = ThreadHelper.getScheduler().submit(new Runnable() {
					@Override
					public void run() {
						try {
						Context c = arg1;
						int dimen = (int) arg1.getResources().getDimension(R.dimen.thumbnailSize);						
						File file = Images.resizeImage(c, dimen, dimen, 90, "thumb", Uri.parse(h.fileName));
						BitmapFactory.Options o = Images.getImageSize(c, Uri.fromFile(file));
						PhotoDao.getInstance().updateThumbnail(h.id, Uri.fromFile(file).toString(), o.outWidth, o.outHeight);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}
		
		private String getId(Cursor c) {
			return c.getString(0);
		}
		
		private String getFileName(Cursor c) {
			return c.getString(1);			
		}
		
		private String getCreated(Cursor c) {
			long created = c.getLong(2);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(created);
			return FORMATTER.format(cal.getTime());			
		}
		
		private String getThumbnail(Cursor c) {
			return c.getString(3);
		}

		@Override
		public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
			View v =  new MenuItemView(arg0);
			ViewHolder h = new ViewHolder();			
			v.setTag(h);
			return v;
		}

	}
	
	private static class ViewHolder {
		Future<?> thumb;
		String fileName;
		String id;
	}

}
