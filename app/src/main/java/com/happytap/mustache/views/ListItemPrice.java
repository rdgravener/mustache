//package com.happytap.mustache.views;
//
//import android.content.Context;
//import android.graphics.drawable.Drawable;
//import android.view.LayoutInflater;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.happytap.mustache.R;
//import com.happytap.mustache.iab.SkuDetails;
//
//public class ListItemPrice extends RelativeLayout {
//
//	private TextView name,description,price;
//
//	public ListItemPrice(Context context) {
//		super(context);
//		LayoutInflater.from(context).inflate(R.layout.view_list_item_purchase,this);
//		name = (TextView) findViewById(R.id.name);
//		description = (TextView) findViewById(R.id.description);
//		price = (TextView) findViewById(R.id.price_container);
//	}
//
//	public void setSkuDetails(String name, SkuDetails dets, boolean purchased) {
//		this.name.setText(name);
//		Drawable drawable = getResources().getDrawable(purchased ?  R.drawable.ic_unlock : R.drawable.ic_lock);
//		if(dets!=null) {
//			description.setText(dets.getDescription());
//			price.setText(dets.getPrice());
//		} else {
//			description.setText("");
//			price.setText("");
//			price.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
//		}
//	}
//
//}
