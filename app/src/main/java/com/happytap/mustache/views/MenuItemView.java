package com.happytap.mustache.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.happytap.mustache.R;

public class MenuItemView extends RelativeLayout {

	private TextView text;
	private View selector;
	private ImageView smartImageView;
	
	private String id;
	
	public MenuItemView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(context).inflate(R.layout.menulist_item,this);
		smartImageView = (ImageView) findViewById(R.id.thumbnail);
		text = (TextView) findViewById(R.id.text);
		selector = findViewById(R.id.selector);
	}

	public MenuItemView(Context context, AttributeSet attrs) {
		this(context,null,-1);
	}

	public MenuItemView(Context context) {
		this(context,null,-1);
	}
	
	public void setSelection(int type) {
		selector.setVisibility(type);
	}
	
	public void setText(String txt) {
		text.setText(txt);
	}
	
	public String getPhotoID() {
		return id;
	}

	public void update(String id, String fileName, String thumbnail, String created) {
		this.id = id;
		text.setText(created);
		if(thumbnail!=null) {
			//smartImageView.set
			//smartImageView.setImageURI(Uri.parse(thumbnail));
		}
	}

}
