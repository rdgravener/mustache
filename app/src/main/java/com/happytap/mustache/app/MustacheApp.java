package com.happytap.mustache.app;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.happytap.mustache.database.DBHelper;
import com.happytap.mustache.service.PhotoService;

/**
 * Created by gravener on 9/13/14.
 */
public class MustacheApp extends Application {

    @Override
    public void onCreate() {
        Log.d("GeniusApp", "onCreate");
        new DBHelper(getApplicationContext(),"mustache.db");
        startService(new Intent(this, PhotoService.class));
    }
}
