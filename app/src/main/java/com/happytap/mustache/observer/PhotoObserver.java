package com.happytap.mustache.observer;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.happytap.mustache.domain.Media;

import java.io.File;

import static org.bytedeco.javacpp.opencv_core.cvLoad;
import static org.bytedeco.javacpp.opencv_highgui.cvLoadImage;

/**
 * Created by gravener on 9/13/14.
 */
public abstract class PhotoObserver extends ContentObserver {

    protected Context ctx;

    public PhotoObserver(Context context) {
        super(null);
        this.ctx = context;
    }

    protected Media readFromMediaStore(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null,
                null, "date_added DESC");
        Media media = null;
        if (cursor.moveToNext()) {
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            String filePath = cursor.getString(dataColumn);
            int mimeTypeColumn = cursor
                    .getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE);
            String mimeType = cursor.getString(mimeTypeColumn);
            media = new Media(new File(filePath), mimeType);
        }
        cursor.close();
        return media;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d("INSTANT", "detected picture");
//        final Media media = readFromMediaStore(ctx,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        new Thread() {
//            @Override
//            public void run() {
//                BitmapFactory.Options o = new BitmapFactory.Options();
//                Resources r = ctx.getResources();
//                BitmapFactory.decodeFile(media.getFile().toString(), o);
//                Log.d("INSTANT", "calculatingInSampleSize");
//                o.inSampleSize = calculateInSampleSize(o,600,600);
//                o.inJustDecodeBounds = false;
//
//            }
//        }.start();
        //saved = "I detected " + media.file.getName();

    }
}
