package com.happytap.mustache.activity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.happytap.mustache.util.IOUtil;
import com.happytap.mustache.util.Images;

import java.io.File;

/**
 * Created by gravener on 9/14/14.
 */
public class MustacheShareToActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        for(String s : intent.getExtras().keySet()) {
            System.out.println(s + " " + intent.getExtras().get(s));
        }
        File cache = new File(IOUtil.getCacheDir(this),"tomustache.jpg");
        if(!cache.getParentFile().exists()) {
            cache.getParentFile().mkdirs();
        }
        try {
            cache.createNewFile();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Uri stream = intent.getData();
        if(stream==null) {
            stream = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        }
        Images.copy(this,stream,cache);
        System.out.println("sent the file to " + cache.getAbsolutePath());
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(cache.getAbsolutePath(),o);
        startActivity(MustachePhotoActivity.from(this, cache,o.outWidth,o.outHeight));
        finish();
    }

}
