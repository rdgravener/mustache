package com.happytap.mustache.activity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.flurry.android.FlurryAgent;
import com.happytap.mustache.R;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.FaceResponse;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.domain.Purchases;
import com.happytap.mustache.fragments.FragmentAbout;
import com.happytap.mustache.fragments.FragmentAd;
import com.happytap.mustache.fragments.FragmentFirstLaunch;
import com.happytap.mustache.fragments.FragmentHistory;
import com.happytap.mustache.fragments.FragmentMustachePhoto;
import com.happytap.mustache.fragments.FragmentMustacheSampler;
import com.happytap.mustache.fragments.FragmentPickPhoto;
import com.happytap.mustache.fragments.FragmentShowPhoto;
import com.happytap.mustache.fragments.FragmentShowPhoto.OnFacesReadyListener;
import com.happytap.mustache.interfaces.OnPhotoPicked;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.ThreadHelper;
import com.happytap.mustache.views.MenuView;
import com.happytap.mustache.views.MenuView.MenuItemClicked;

public class MustacheActivity extends ActionBarActivity implements MenuItemClicked, OnFacesReadyListener {

	private OnPhotoPicked onPhotoPicked;
	
	FragmentPickPhoto pickPhoto;
	FragmentShowPhoto showPhoto;
	
	String currentFragmentTag;
	
	Handler handler = new Handler();
	
	SharedPreferences prefs;
	
	DrawerLayout drawer;
	
	ActionBarDrawerToggle toggle;
	
	MenuView menuView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mustache);
		drawer = (DrawerLayout) findViewById(R.id.drawer);
		toggle = new ActionBarDrawerToggle(this, drawer, R.drawable.ic_navigation_drawer, R.string.a_happytap_production, R.string.a_happytap_production);
		drawer.setDrawerListener(toggle);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		onPhotoPicked = new OnPhotoPicked() {
			@Override
			public void onPhoto(Photo photo) {				
				//pickPhoto.setVisibility(View.GONE);
				//showPhoto.setVisibility(View.VISIBLE);
				showPhoto = new FragmentShowPhoto();
				showPhoto.setOnFacesReadyListener(MustacheActivity.this);
				showPhoto.setPhoto(photo);
				getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, showPhoto,FragmentPickPhoto.class.getName()).commit();
				currentFragmentTag = FragmentShowPhoto.class.getName();
			}
		};
		pickPhoto = new FragmentPickPhoto();
		pickPhoto.setOnPhotoPicked(onPhotoPicked);
		FragmentMustacheSampler sampler = new FragmentMustacheSampler();
		getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, sampler,FragmentMustacheSampler.class.getName()).commit();
		currentFragmentTag = FragmentMustacheSampler.class.getName();
		
		getSupportActionBar().setHomeButtonEnabled(true);
		
		if(!prefs.contains(Purchases.REMOVE_ADS.getSku())) {
			getSupportFragmentManager().beginTransaction().replace(R.id.ad_container, new FragmentAd(),FragmentAd.class.getName()).commit();
		}
		//getSupportFragmentManager().beginTransaction().replace(R.id.leftFragment, new FragmentLeftMenu()).commit();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		menuView = (MenuView) findViewById(R.id.menu_view);
		menuView.setOnMenuItemClicked(this);
	}
	
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		toggle.syncState();
	};
	
	public static float convertPixelsToDp(float px,Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;

	}
	
	public static float convertDpToPixel(float dp,Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi/160f);
	    return px;
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		toggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void setContentView(int id) {
		setContentView(getLayoutInflater().inflate(id, null));
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#setContentView(android.view.View)
	 */
	@Override
	public void setContentView(View v) {
		setContentView(v, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
	 */
	@Override
	public void setContentView(View v, LayoutParams params) {
		super.setContentView(v, params);
	}

	@Override
	public void onMustache(Photo photo) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("Hash", photo.hash);
		map.put("Id", photo.id);
		map.put("Faces_Checked", String.valueOf(photo.facesChecked));
		map.put("Width", String.valueOf(photo.width));
		map.put("Height", String.valueOf(photo.height));
		FlurryAgent.logEvent("Photo_Clicked",map);
		if(drawer.isDrawerOpen(Gravity.LEFT)) {
			handler.postDelayed(toggler, 250);
		}
//		if(TextUtils.isEmpty(photo.url)) {
//			if(showPhoto==null) {
//				showPhoto = new FragmentShowPhoto();
//			}
//			showPhoto.setPhoto(photo);
//			getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, showPhoto).commit();
//			currentFragmentTag = FragmentShowPhoto.class.getName();
//			FlurryAgent.logEvent("Photo_Show_Needs_Face_Detection",map);
//			return;
//		}
		if(photo.facesChecked) {
			FragmentMustachePhoto fmp = new FragmentMustachePhoto();
			fmp.setPhoto(photo);
			getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, fmp).commit();
			currentFragmentTag = FragmentMustachePhoto.class.getName();
			FlurryAgent.logEvent("Photo_Show_Mustaches",map);
			return;
		}		
	}
	
	@Override
	public void onNav(int id) {
		if(drawer.isDrawerOpen(Gravity.LEFT)) {
			handler.postDelayed(toggler, 250);
		}
		Fragment fragment = null;
		if(id==R.drawable.ic_history) {
			FragmentHistory h = new FragmentHistory();
			h.setOnMenuItemClicked(this);
			fragment = h;
			FlurryAgent.logEvent("History_Clicked");
		} else
		if(id==R.drawable.ic_action_about) {
			FragmentAbout a = new FragmentAbout();
			fragment = a;
			FlurryAgent.logEvent("About_Clicked");
		}
		if(id==R.drawable.ic_action_help) {
			FragmentFirstLaunch f = new FragmentFirstLaunch();
			fragment = f;
			FlurryAgent.logEvent("Help_Clicked");
		}		
		getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, fragment).commit();		
		currentFragmentTag = fragment.getClass().getName();
		invalidateOptionsMenu();
	}
	;
	
	@Override
	public void onOther(Object o) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onFaces(Photo photo, FaceResponse resp) {
		onMustache(photo);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(toggle.onOptionsItemSelected(item)) {
			return true;
		}
		if(item.getItemId()==R.id.menu_from_camera) {
			onTake();
		}
		if(item.getItemId()==R.id.menu_from_gallery) {
			onPick();
		}
		if(currentFragmentTag!=null) {
			Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
			if(fragment!=null) {
				fragment.onOptionsItemSelected(item);
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, final Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == PHOTO_ACTION_TAKE || requestCode == PHOTO_ACTION_PICK) {
				final String fileName = _currentPhotoPath;
				ThreadHelper.getScheduler().submit(new Runnable() {
					@Override
					public void run() {
						final Photo p = new Photo();
						p.created = System.currentTimeMillis();
						p.id = UUID.randomUUID().toString();
						if(data!=null) {							
							p.fileName = data.getDataString();
							try {
								File file = createImageFile();
								String hash = Images.copy(MustacheActivity.this, data.getData(), file);
								if(hash==null) {
									throw new RuntimeException();
								}
								p.hash = hash;
								p.fileName = Uri.fromFile(file).toString();
							} catch (IOException e) {
								throw new RuntimeException(e);
							}
						} else {
							p.fileName = fileName;
						}
						PhotoDao.getInstance().savePhoto(p);
						
						if(onPhotoPicked!=null) {
							runOnUiThread(new Runnable() {
								public void run() {
									onPhotoPicked.onPhoto(p);
								};
							});
							
						}
					}
				});
			}
		}
	}

	
	private void onTake() {
		Intent takePictureIntent = new Intent(
				MediaStore.ACTION_IMAGE_CAPTURE);
		try {
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					Uri.fromFile(createImageFile()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		startActivityForResult(takePictureIntent, PHOTO_ACTION_TAKE);
	}
	
	private String _currentPhotoPath;
	
	private static final int PHOTO_ACTION_TAKE = 100;
	private static final int PHOTO_ACTION_PICK = 200;
	
	private static final String JPEG_FILE_PREFIX = "mustache_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";
	
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp;

		File image = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX,
				getAlbumDir(this));
		_currentPhotoPath = Uri.fromFile(image).toString();
		return image;
	}
	
	public static File getAlbumDir(Context ctx) {
		File file = new File(Environment.getExternalStorageDirectory(),
				"Android/data/" + ctx.getPackageName() + "/files/");
		String state = Environment.getExternalStorageState();
		System.out.println(state);
		if (!file.exists()) {
			System.out.println(file);
			System.out.println(file.mkdirs());
		}
		return file;
	}

	private void onPick() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(intent, PHOTO_ACTION_PICK);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {				
		if(currentFragmentTag!=null) {
			Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
			if(fragment!=null) {
				fragment.onCreateOptionsMenu(menu, getMenuInflater());
			}
		}
		getMenuInflater().inflate(R.menu.menu_mustache, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, "R5X59W7DHC9Q7FDJ462H");
	}
	
	Runnable toggler = new Runnable() {
		@Override
		public void run() {
			drawer.closeDrawers();
		}
	}; 
	
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
}
