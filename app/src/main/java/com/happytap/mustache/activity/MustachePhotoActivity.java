package com.happytap.mustache.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.happytap.mustache.R;
import com.happytap.mustache.domain.FaceSimple;
import com.happytap.mustache.observer.PhotoObserver;
import com.happytap.mustache.util.IOUtil;
import com.happytap.mustache.util.PhotoUtil;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Created by gravener on 9/14/14.
 */
public class MustachePhotoActivity extends FragmentActivity {

    File mustacheFile;

    ImageView image;

    int originalWidth,originalHeight;

    Bitmap bitmap;

    List<FaceSimple> faces = Collections.emptyList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mustache_photo);
        mustacheFile = (File) getIntent().getSerializableExtra("file");
        originalWidth = getIntent().getIntExtra("width",0);
        originalHeight = getIntent().getIntExtra("height",0);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,MustachePhotoFragment.newInstance(mustacheFile,originalWidth,originalHeight)).commit();

    }


    public static Intent from(Context ctx, File mustacheFile, int width, int height) {
        Intent intent = new Intent(ctx,MustachePhotoActivity.class);
        intent.putExtra("file",mustacheFile);
        intent.putExtra("width", width);
        intent.putExtra("height",height);
        return intent;
    }
}
