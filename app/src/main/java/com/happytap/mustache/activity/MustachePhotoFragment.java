package com.happytap.mustache.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ScaleGestureDetectorCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.happytap.mustache.R;
import com.happytap.mustache.domain.FaceSimple;
import com.happytap.mustache.domain.PointF;
import com.happytap.mustache.observer.PhotoObserver;
import com.happytap.mustache.util.PhotoUtil;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Created by gravener on 9/15/14.
 */
public class MustachePhotoFragment extends Fragment {

    int originalWidth;
    int originalHeight;
    File mustacheFile;
    Bitmap bitmap;
    ImageView image;

    List<FaceSimple> faces = Collections.emptyList();

    RelativeLayout root;
    RelativeLayout imageContainer;

    Handler handler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (RelativeLayout) inflater.inflate(R.layout.fragment_mustache_photo,container,false);
        return root;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle a = getArguments();
        mustacheFile = (File)a.getSerializable("file");
        originalHeight = a.getInt("height");
        originalWidth = a.getInt("width");
        importImage(mustacheFile);
    }

    public static MustachePhotoFragment newInstance(File mustache, int width, int height) {
        MustachePhotoFragment f = new MustachePhotoFragment();
        Bundle b = new Bundle();
        b.putSerializable("file",mustache);
        b.putInt("width",width);
        b.putInt("height",height);
        f.setArguments(b);
        return f;
    }

    private void importImage(File mustacheFile) {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mustacheFile.getAbsolutePath(),o);
        this.originalWidth = o.outWidth;
        this.originalHeight = o.outHeight;
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int inSampleSize = PhotoObserver.calculateInSampleSize(o, dm.widthPixels, dm.heightPixels);
        o.inJustDecodeBounds = false;
        o.inSampleSize = inSampleSize;
        o.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmap = BitmapFactory.decodeFile(mustacheFile.getAbsolutePath());
        if(image==null) {
            imageContainer = new RelativeLayout(getActivity());
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(bitmap.getWidth(),bitmap.getHeight());
            root.addView(imageContainer, lp);
            image = new ImageView(getActivity());
            RelativeLayout.LayoutParams lpimage = new RelativeLayout.LayoutParams(bitmap.getWidth(),bitmap.getHeight());
            imageContainer.addView(image,lpimage);
        }
        image.setImageBitmap(bitmap);

        autoMustachePhoto(mustacheFile);
    }

    private void autoMustachePhoto(File mustacheFile) {
        List<FaceSimple> faces = PhotoUtil.mustache(getActivity(), mustacheFile);
        float percentage = bitmap.getWidth() / (float) originalWidth;
        for(FaceSimple face : faces) {
            System.out.println(face);
            face.recalibrateFace(percentage);
            System.out.println(face);
        }
        this.faces = faces;
        //image.invalidate();
        setupMustaches(faces);
    }

    private void setupMustaches(List<FaceSimple> faces) {
        imageContainer.removeViews(1,imageContainer.getChildCount()-1);


        int position = 1;
        int resId = getResources().getIdentifier("mustache"+position, "raw", getActivity().getPackageName());

        Bitmap mustache = BitmapFactory.decodeStream(getResources().openRawResource(resId));
        float scale =  1f;


        for(FaceSimple f : faces) {
            View v = new View(getActivity());
            v.setBackgroundColor(Color.RED);
            RelativeLayout.LayoutParams lp;
//            imageContainer.addView(v,lp);
//            v = new View(getActivity());
//            v.setBackgroundColor(Color.BLUE);
//            lp = new RelativeLayout.LayoutParams(16, 16);
//            lp.leftMargin = (int)(f.getMouthRight().x - 8);
//            lp.topMargin = (int)(f.getMouthRight().y - 8);
//            imageContainer.addView(v,lp);


            PointF left = f.getMouthLeft();
            int newLeftX = (int) (left.x*scale);
            int newLeftY = (int) (left.y*scale);
            PointF right = f.getMouthRight();
            int newRightX = (int) (right.x*scale);
            int newRightY = (int) (right.y*scale);
            PointF nose = f.getNose();
            int newNoseY = (int)(f.getNose().y*scale);

            float deltaY = newRightY-newLeftY;
            float deltaX = newRightX - newLeftX;

            float angle = (float) (Math.atan2(deltaY, deltaX) * 180 / Math.PI);

            System.out.println(angle);

            Matrix matrix = new Matrix();
            matrix.setRotate(angle,0,0);

            android.graphics.PointF k = new android.graphics.PointF(newLeftX,newLeftY);
            double hypotenuse = Math.sqrt(Math.pow(newRightX-newLeftX, 2)+Math.pow(newRightX-newLeftY, 2));
            double noseOffset = Math.sin(Math.toRadians(Math.abs(angle)))*hypotenuse;
            int resourceId = getResources().getIdentifier("mustache"+position+"_width_scale", "integer", getActivity().getPackageName());
            float xScale = (getResources().getInteger(resourceId)/100f)/2f;
            newLeftX*=(1-xScale);
            newRightX*=(1+xScale);
            int width = newRightX - newLeftX;
            float mustacheScale = (width / (float) mustache.getWidth());
            int max = (int)Math.hypot((double)mustache.getWidth(), (double)mustache.getHeight());
            //int diffWidth =  tmp.getWidth()-mustache.getWidth();
            //int diffHeight = tmp.getHeight()-mustache.getHeight();
//				int offsetX = 0;
//				int offsetY = 0;
//				if(diffWidth>0) {
//					offsetX = diffWidth/2;
//				} else {
//					offsetY = diffHeight/2;
//				}
//				for(int i = 0; i < mustache.getWidth(); i++) {
//					for(int j = 0; j < mustache.getHeight(); j++) {
//						tmp.setPixel(offsetX+i, offsetY+j, mustache.getPixel(i, j));
//					}
//				}
            Bitmap rotated = Bitmap.createBitmap(mustache,0,0,mustache.getWidth(),mustache.getHeight(),matrix,true);
            final Bitmap newStache = Bitmap.createScaledBitmap(rotated, width, (int)(mustacheScale*rotated.getHeight()), true);
            System.out.println(newStache.getWidth()+"x"+newStache.getHeight());
            newNoseY-=noseOffset;
            resourceId = getResources().getIdentifier("mustache"+position+"_y_offset", "integer", getActivity().getPackageName());
            final int newY = newNoseY + (int)((newLeftY-newNoseY)*getResources().getInteger(resourceId)/100f);
            final int nnL = newLeftX;

            final ImageView i = new ImageView(getActivity());
            i.setClickable(false);
            i.setOnTouchListener(new View.OnTouchListener() {

                ScaleGestureDetector.OnScaleGestureListener scaleListener = new ScaleGestureDetector.OnScaleGestureListener() {
                    @Override
                    public boolean onScale(ScaleGestureDetector detector) {
                        System.out.println("onScale");
                        return false;
                    }

                    @Override
                    public boolean onScaleBegin(ScaleGestureDetector detector) {
                        System.out.println("onScaleBegin");
                        return false;
                    }

                    @Override
                    public void onScaleEnd(ScaleGestureDetector detector) {
                        System.out.println("onScaleEnd");
                    }
                };

                ScaleGestureDetector detector = new ScaleGestureDetector(getActivity(), scaleListener);


                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    System.out.println("onTouch");
                    detector.onTouchEvent(event);
                    return false;
                }
            });
            i.setImageBitmap(newStache);
            lp = new RelativeLayout.LayoutParams(newStache.getWidth(), newStache.getHeight());
            lp.leftMargin = nnL;
            lp.topMargin = newY;
            imageContainer.addView(i,lp);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Rect r = new Rect();
                    i.getHitRect(r);
                    r.left-=200;
                    r.right+=200;
                    r.top-=200;
                    r.bottom+=200;
                    TouchDelegate touchDelegate = new TouchDelegate(r,
                            i);
                    imageContainer.setTouchDelegate(touchDelegate);
                }
            });

        }
    }



}
