package com.happytap.mustache.fragments;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class FragmentAd extends Fragment {

	RelativeLayout container;
	private AdView adView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.container = new RelativeLayout(inflater.getContext());
		return this.container;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if(adView!=null) {
			container.removeAllViews();
		}
		AdSize s = AdSize.SMART_BANNER;
		Bundle args = getArguments();
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		if(args!=null) {
			s = AdSize.MEDIUM_RECTANGLE;
			container.setBackgroundColor(0x55000000);
			lp.addRule(RelativeLayout.CENTER_VERTICAL);
		}
        adView = new AdView(getActivity());
        adView.setAdSize(s);
        adView.setAdUnitId("a14fcce6f8587ce");
		
		lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		adView.setVisibility(View.GONE);
		container.addView(adView,lp);
		
		
		
		adView.setAdListener(new AdListener() {

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                FlurryAgent.logEvent("Ad_Failed");
                FlurryAgent.endTimedEvent("Ad_Requested");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
                FlurryAgent.logEvent("Ad_Displayed");
                FlurryAgent.endTimedEvent("Ad_Requested");
            }

		});
		Map<String,String> m = new HashMap<String,String>();
		if(args!=null) {			
			m.put("Paging_Ad", Boolean.TRUE.toString());
		} else {
			m.put("Paging_Ad", Boolean.FALSE.toString());
		}
//		m.put("Ad_Size", s.toString());
		FlurryAgent.logEvent("Ad_Requested", m,true);
		adView.loadAd(new AdRequest.Builder().build());
	};
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(adView!=null) {
			FlurryAgent.endTimedEvent("Ad_Requested");
			adView.destroy();
		}
	}
	
}
