package com.happytap.mustache.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.happytap.mustache.R;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.views.MenuItemView;
import com.happytap.mustache.views.MenuView.MenuItemClicked;

public class FragmentHistory extends Fragment {

	View root;
	GridView list;
	
	private MenuItemClicked onMenuItemClicked;
	
	public void setOnMenuItemClicked(MenuItemClicked onMenuItemClicked) {
		this.onMenuItemClicked = onMenuItemClicked;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.fragment_history,container,false);
		list = (GridView) root.findViewById(R.id.list);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				MenuItemView v = (MenuItemView) arg1;
				Photo photo = PhotoDao.getInstance().getPhoto(v.getPhotoID());
				if(onMenuItemClicked!=null) {
					onMenuItemClicked.onMustache(photo);
				}
			}
		});
		return root;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		list.setAdapter(new HistoryAdapter(getActivity()));
	}
	
	
	
}
