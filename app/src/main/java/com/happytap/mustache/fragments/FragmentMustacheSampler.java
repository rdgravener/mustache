package com.happytap.mustache.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.happytap.mustache.activity.MustacheActivity;
import com.happytap.mustache.R;

public class FragmentMustacheSampler extends Fragment {

	ViewPager pager;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_mustache_overlay, container,false);
		pager = new ViewPager(inflater.getContext());
		pager.setId(0xdddd);
		pager.setPageMargin((int)getResources().getDimension(R.dimen.mustache_next));
		root.addView(pager);
		return root;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		pager.setAdapter(new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
			@Override
			public int getCount() {
				return 27;
			}
			
			@Override
			public Fragment getItem(int arg0) {
				FragmentMustacheSamplerSampler p = new FragmentMustacheSamplerSampler();
				Bundle b = new Bundle();
				b.putInt("position", arg0);
				p.setArguments(b);
				return p;
			}			
		});
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageSelected(int arg0) {
				MustacheActivity a = (MustacheActivity) getActivity();
			}
		});
		
		pager.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//getActivity().
			}
		});
	}
	
}
