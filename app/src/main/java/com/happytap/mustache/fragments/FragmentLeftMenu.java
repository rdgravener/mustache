package com.happytap.mustache.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.happytap.mustache.R;

public class FragmentLeftMenu extends Fragment {

	ListView list;
	
	OnItemClickListener onItemClickListener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_left_menu, null);
		list = (ListView) root.findViewById(R.id.list);
		return root;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		list.setAdapter(new FragmentLeftMenuAdapter(getActivity()));
		list.setOnItemClickListener(onItemClickListener);
	}
	
	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}
	
}
