package com.happytap.mustache.fragments;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.happytap.mustache.R;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.FaceResponse;
import com.happytap.mustache.domain.FaceSimple;
import com.happytap.mustache.domain.Media;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.PhotoUtil;
import com.happytap.mustache.util.ThreadHelper;

public class FragmentShowPhoto extends Fragment {

	private ViewGroup root;

	private ImageView image;

	private View addMustache;
	
	private View addAutoMustache;
	private View addManualMustache;

	private TextView progressBar;

	private Photo _photo;

	private Photo _scaledPhoto;

	private Future<?> loadFile;

	private Future<?> uploadPhotoFuture;

	private Future<?> getFaceCoordinatesFuture;

	private Object sync = new Object();

	private double _percent;
 
	private static DecimalFormat FORMATTER = new DecimalFormat("#.##%");
	
	private TextView countDown;
	
	private Handler handler = new Handler();
	
	public interface OnFacesReadyListener {
		void onFaces(Photo photo, FaceResponse resp);
	}
	
	private OnFacesReadyListener onFacesReadyListener;
	
	public void setOnFacesReadyListener(
			OnFacesReadyListener onFacesReadyListener) {
		this.onFacesReadyListener = onFacesReadyListener;
	}

	public void setPhoto(Photo photo) {
		_photo = photo;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = (ViewGroup) inflater.inflate(R.layout.fragment_show_photo,
				container, false);
		image = (ImageView) root.findViewById(R.id.image);
		addMustache = root.findViewById(R.id.add_mustache);
		addAutoMustache = addMustache.findViewById(R.id.add_mustache_auto);
		addManualMustache = addMustache.findViewById(R.id.add_mustache_manual);
		progressBar = (TextView) root.findViewById(R.id.progress_bar);
		countDown = (TextView) root.findViewById(R.id.countdown);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("photo")) {
				setPhoto((Photo) savedInstanceState.getSerializable("photo"));
			}
		}
		addAutoMustache.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (TextUtils.isEmpty(_photo.url)) {
					if (uploadPhotoFuture != null) {
						uploadPhotoFuture.cancel(true);
					}
					uploadPhotoFuture = ThreadHelper.getScheduler().submit(
							localFaceDetect);
				}
			}
		});
		loadFile();
//		AnimationSet s = new AnimationSet(false);
//		
//		AlphaAnimation a = new AlphaAnimation(1.0f, 0.2f);
//		a.setAnimationListener(new AnimationListener() {
//			
//			int time = 3;
//			
//			@Override
//			public void onAnimationEnd(Animation animation) {
//				countDown.setVisibility(View.GONE);
//			}
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//				// TODO Auto-generated method stub
//				time--;
//				countDown.setText(String.valueOf(time));
//				
//			}
//			
//			@Override
//			public void onAnimationStart(Animation animation) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		AlphaAnimation b = new AlphaAnimation(0.2f, 1.0f);
//		b.setRepeatCount(1);
//		b.setDuration(500);
//		a.setRepeatCount(1);
//		a.setDuration(500);
//		s.addAnimation(a);
//		s.addAnimation(b);
//		s.addAnimation(a);
//		s.addAnimation(b);
//		countDown.setAnimation(s);
//		s.start();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (_photo != null) {
			outState.putSerializable("photo", _photo);
		}
	}

	private void loadFile() {
		if (loadFile != null) {
			loadFile.cancel(true);
		}
		if (_photo == null) {
			return;
		}
		loadFile = ThreadHelper.getScheduler().submit(new Runnable() {
			@Override
			public void run() {
				synchronized (sync) {

					try {
						BitmapFactory.Options options = Images.getImageSize(
								getActivity(), Uri.parse(_photo.fileName));
						_photo.width = options.outWidth;
						_photo.height = options.outHeight;
						PhotoDao.getInstance().savePhoto(_photo);

						int maxWidth = root.getMeasuredWidth();
						int maxHeight = root.getMeasuredHeight();

						float percentXOff = _photo.width / (float) maxWidth;
						float percentYOff = _photo.height / (float) maxHeight;

						final int newWidth, newHeight;

						final float scale;

						int ratioVt = _photo.width / maxHeight;
						int ratioHz = _photo.height / maxWidth;

						if (percentYOff < percentXOff) {
							scale = maxWidth / (float) _photo.width;
						} else {
							scale = maxHeight / (float) _photo.height;
						}

						newWidth = Math.round(_photo.width * scale);
						newHeight = Math.round(_photo.height * scale);
						System.out.println(String.format(
								"percentXOff %s, percentYOff %s", percentXOff,
								percentYOff));
						System.out.println(String
								.format("maxWidth: %s, maxHeight: %s, scale: %s, w: %s, h: %s, nw: %s, nh: %s",
										maxWidth, maxHeight, scale,
										_photo.width, _photo.height, newWidth,
										newHeight));

						// options.inJustDecodeBounds = false;
						// options.inSampleSize = scale * 100;

						// ImageInfo imageInfo = new ImageInfo(_photo.fileName);
						// MagickImage magickImage = new MagickImage(imageInfo);
						// magickImage.scaleImage(newWidth, newHeight);
						// _scaledPhoto = new Photo();
						// int extPos = _photo.fileName.lastIndexOf(".");
						// String newName = _photo.fileName.substring(0, extPos)
						// + String.format("_%sx%s",newWidth,newHeight) +
						// ".jpg";
						// _scaledPhoto.fileName = newName;
						// _scaledPhoto.width = newWidth;
						// _scaledPhoto.height = newHeight;
						int newScale = 0;

						final Bitmap bitmap = Images.loadImage(getActivity(),
								(int) (maxWidth * 1.3),
								(int) (maxHeight * 1.3),
								Uri.parse(_photo.fileName));
						System.out.println(String.format(
								"w: %s, h: %s, scale: %s", bitmap.getWidth(),
								bitmap.getHeight(), newScale));
						handler.post(new Runnable() {
							@Override
							public void run() {
								image.setImageBitmap(bitmap);
								addMustache.setVisibility(View.VISIBLE);
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		});
	}

	public void setVisibility(int gone) {
		if (gone == View.VISIBLE && root.getVisibility() != View.VISIBLE) {
			loadFile();
		}
		root.setVisibility(gone);

	}

    private Runnable localFaceDetect = new Runnable() {
        @Override
        public void run() {

            try {
                Media m = new Media(new File(_photo.fileName.replace("file://","")),"image/jpeg");
                List<FaceSimple> faces = PhotoUtil.mustache(getActivity(), m);
                final FaceResponse r = FaceResponse.newFaceResponse(faces);
                System.out.println("there are " + faces.size() + " faces ");
                PhotoDao.getInstance().saveFace(_photo.id, r);
                _photo.facesChecked = true;
                PhotoDao.getInstance().savePhoto(_photo);
                if(!r.getFaces().isEmpty()) {
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if(onFacesReadyListener!=null) {
                                onFacesReadyListener.onFaces(_photo, r);
                            }
                        };
                    }, 3000);
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setText("Face detection only works with humans.  We couldn't find any faces in this photo.");
                        };
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();;
            }

        }
    };

	private Runnable faceDetect = new Runnable() {
		public void run() {
			parms.put("url", _photo.url);
			FlurryAgent.logEvent("Face_Detect",parms,true);
			Uri u = Uri.parse("http://rekognition.com/func/api/");
			u = u.buildUpon()
					.appendQueryParameter("api_key", "HndeyexD9kF45VSL")
					.appendQueryParameter("api_secret", "rz6zg3HktEuPiisQ")
					.appendQueryParameter("urls", _photo.url)
					.appendQueryParameter("jobs", "face_age_glass_part_age")
					.build();

			HttpURLConnection conn = null;
			handler.post(new Runnable() {
				public void run() {
					progressBar.setText("Detecting faces...");
				};
			});
			
			try {
				URL url = new URL(u.toString());
				conn = (HttpURLConnection) url.openConnection();
				StringBuilder b = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = null;
				while ((line = br.readLine()) != null) {
					b.append(line).append('\n');
				}
				JSONObject o = new JSONObject(b.toString());
				final FaceResponse r = new FaceResponse(o);
				
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						progressBar.setText(String.format("Found %s %s",r.getFaces().size(),r.getFaces().size()==1?"face":"faces"));
					};
				});
				parms.put("Faces_Response", b.toString());
				parms.put("Number_Of_Faces", String.valueOf(r.getFaces().size()));
				parms.put("Face_Api_Quota", String.valueOf(r.getQuota()));
				FlurryAgent.logEvent("Faces_Detected",parms);
				PhotoDao.getInstance().saveFace(_photo.id, r);
				_photo.facesChecked = true;
				PhotoDao.getInstance().savePhoto(_photo);
				if(!r.getFaces().isEmpty()) {
					handler.postDelayed(new Runnable() {
						public void run() {
							if(onFacesReadyListener!=null) {
								onFacesReadyListener.onFaces(_photo, r);
							}
						};
					}, 3000);					
				} else {
					handler.post(new Runnable() {
						public void run() {
							progressBar.setText("Face detection only works with humans.  We couldn't find any faces in this photo.");
						};
					});
				}
			} catch (Exception e) {
				parms.put("Exception", e.getMessage());
				FlurryAgent.logEvent("Face_Detect_Error", parms);
				handler.post(new Runnable() {
					public void run() {
						progressBar.setText("Some sort of error happened.  We were to lazy to figure out what error, so check your internet connection and try again.");
					};
				});
			} finally {
				FlurryAgent.endTimedEvent("Face_Detect");
			}
		};
	};

	Map<String,String> parms = new HashMap<String,String>();

	
	private void showUploadError(Exception e) {
		
	}

	private Runnable updateProgress = new Runnable() {
		@Override
		public void run() {
			if (progressBar.getVisibility() != View.VISIBLE) {
				progressBar.setVisibility(View.VISIBLE);
				progressBar.setText(String.valueOf(FORMATTER.format(_percent))
						+ " uploaded.");
			}
		}
	};

	private Runnable updateDetecting = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub

		}
	};
}
