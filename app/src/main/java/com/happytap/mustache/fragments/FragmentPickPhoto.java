package com.happytap.mustache.fragments;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.happytap.mustache.R;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.interfaces.OnPhotoPicked;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.ThreadHelper;

public class FragmentPickPhoto extends Fragment {

	private View pickPhoto, takePhoto;

	private String _currentPhotoPath;

	private static final int PHOTO_ACTION_TAKE = 100;
	private static final int PHOTO_ACTION_PICK = 200;

	private static final String JPEG_FILE_PREFIX = "mustache_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";
	
	private OnPhotoPicked onPhotoPicked;
	
	private View root;
	
	public void setOnPhotoPicked(OnPhotoPicked onPhotoPicked) {
		this.onPhotoPicked = onPhotoPicked;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_mustache_picker, container, false);
		pickPhoto = v.findViewById(R.id.pick_photo);
		takePhoto = v.findViewById(R.id.take_photo);
		root = v;
		return v;
	}
//
//	private Runnable faceDetect = new Runnable() {
//		public void run() {
//			Uri u = Uri.parse("http://rekognition.com/func/api/");
//			u = u.buildUpon().appendQueryParameter("api_key", "HndeyexD9kF45VSL")
//			.appendQueryParameter("api_secret", "rz6zg3HktEuPiisQ")
//			.appendQueryParameter("urls", "https://s3.amazonaws.com/happymustache/1e6cf613-7396-4ca9-bc07-e7d7e96ea981.jpg")
//			.appendQueryParameter("jobs", "face_age_glass_part_age").build();
//			
//			HttpURLConnection conn = null;
//			
//			try {
//				URL url = new URL(u.toString());
//				conn = (HttpURLConnection) url.openConnection();
//				StringBuilder b = new StringBuilder();
//				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//				String line = null;
//				while((line = br.readLine())!=null) {
//					b.append(line).append('\n');
//				}
//				JSONObject o = new JSONObject(b.toString());
//				FaceResponse r = new FaceResponse(o);
//				System.out.println(r);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		};
//	};
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		pickPhoto.setOnClickListener(onClicked);
		takePhoto.setOnClickListener(onClicked);
		
		if(savedInstanceState!=null) {
			if(savedInstanceState.containsKey("currentPhotoPath")) {
				_currentPhotoPath = savedInstanceState.getString("currentPhotoPath");
			}
		}
//		ThreadHelper.getScheduler().submit(faceDetect);
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, final Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == PHOTO_ACTION_TAKE || requestCode == PHOTO_ACTION_PICK) {
				final String fileName = _currentPhotoPath;
				ThreadHelper.getScheduler().submit(new Runnable() {
					@Override
					public void run() {
						final Photo p = new Photo();
						p.created = System.currentTimeMillis();
						p.id = UUID.randomUUID().toString();
						if(data!=null) {							
							p.fileName = data.getDataString();
							try {
								File file = createImageFile();
								String hash = Images.copy(getActivity(), data.getData(), file);
								if(hash==null) {
									throw new RuntimeException();
								}
								p.hash = hash;
								p.fileName = Uri.fromFile(file).toString();
							} catch (IOException e) {
								throw new RuntimeException(e);
							}
						} else {
							p.fileName = fileName;
						}
						PhotoDao.getInstance().savePhoto(p);
						
						if(onPhotoPicked!=null) {
							getActivity().runOnUiThread(new Runnable() {
								public void run() {
									onPhotoPicked.onPhoto(p);
								};
							});
							
						}
					}
				});
			}
		}
	}

	private OnClickListener onClicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == pickPhoto) {
				onPick();
			}
			if (v == takePhoto) {
				onTake();
			}
		}

		private void onTake() {
			Intent takePictureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);
			try {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(createImageFile()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			startActivityForResult(takePictureIntent, PHOTO_ACTION_TAKE);
		}

		private void onPick() {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(intent, PHOTO_ACTION_PICK);

		}
	};
	
	public void onSaveInstanceState(Bundle outState) {
		if(_currentPhotoPath!=null) {
			outState.putString("currentPhotoPath", _currentPhotoPath);
		}
	};

	public static File getAlbumDir(Context ctx) {
		File file = new File(Environment.getExternalStorageDirectory(),
				"Android/data/" + ctx.getPackageName() + "/files/");
		String state = Environment.getExternalStorageState();
		System.out.println(state);
		if (!file.exists()) {
			System.out.println(file);
			System.out.println(file.mkdirs());
		}
		return file;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp;

		File image = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX,
				getAlbumDir(getActivity()));
		_currentPhotoPath = Uri.fromFile(image).toString();
		return image;
	}

	public void setVisibility(int gone) {
		root.setVisibility(gone);
	}

}
