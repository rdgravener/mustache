package com.happytap.mustache.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.happytap.mustache.R;
import com.happytap.mustache.util.ThreadHelper;

public class FragmentMustacheSamplerSampler extends Fragment {

	RelativeLayout root;
	ImageView i;
	ImageView next;
	ImageView previous;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = new RelativeLayout(inflater.getContext());
		root.setPadding(20, 20, 20, 20);
		i = new ImageView(inflater.getContext());
		i.setAdjustViewBounds(true);
		next = new ImageView(inflater.getContext());
		previous = new ImageView(inflater.getContext());
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		root.addView(i, lp);
		int width = (int) getResources().getDimension(R.dimen.mustache_next);
		RelativeLayout l = new RelativeLayout(inflater.getContext());
		lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		root.addView(l,lp);
		lp = new RelativeLayout.LayoutParams(width, width);
		lp.setMargins(10, 0, 10, 0);
		previous.setId(100);
		l.addView(previous, lp);
		lp = new RelativeLayout.LayoutParams(width, width);
		lp.setMargins(10, 0, 10, 0);
		lp.addRule(RelativeLayout.RIGHT_OF,100);
		l.addView(next, lp);
		
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Bundle args = getArguments();
		int mustache = args.getInt("position") + 1;

		final int resourceId = getResources().getIdentifier(
				"mustache" + mustache, "raw", getActivity().getPackageName());
		final int nextId = getResources().getIdentifier(
				"mustache" + (mustache + 1), "raw",
				getActivity().getPackageName());
		final int prevId = getResources().getIdentifier(
				"mustache" + (mustache - 1), "raw",
				getActivity().getPackageName());

		ThreadHelper.getScheduler().submit(new Runnable() {
			@Override
			public void run() {
				final Bitmap bitmap = BitmapFactory.decodeStream(getResources()
						.openRawResource(resourceId));
				final Bitmap nextb;
				final Bitmap prevb;
				if (nextId != 0) {
					nextb = BitmapFactory.decodeStream(getResources()
							.openRawResource(nextId));
				} else {
					nextb = null;
				}
				if (prevId != 0) {
					prevb = BitmapFactory.decodeStream(getResources()
							.openRawResource(prevId));
				} else {
					prevb = null;
				}

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						i.setImageBitmap(bitmap);
						next.setImageBitmap(nextb);
						next.setVisibility(nextb==null?View.GONE:View.VISIBLE);
						previous.setImageBitmap(prevb);
						previous.setVisibility(prevb==null?View.GONE:View.VISIBLE);
					}
				});
			}
		});

	}

}
