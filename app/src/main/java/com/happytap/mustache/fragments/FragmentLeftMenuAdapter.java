package com.happytap.mustache.fragments;


import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.happytap.mustache.R;

public class FragmentLeftMenuAdapter extends BaseAdapter {

	private String[] menus = new String[]{"History", /*"Settings",*/ "Unlock", "About", "Help"};
	private int[] drawables = new int[]{R.drawable.ic_history, /*R.drawable.ic_settings,*/ R.drawable.ic_unlock, R.drawable.ic_action_about, R.drawable.ic_action_help};
	
	private Integer selectedDrawable;
	
	private Typeface robotoThin;
	
	public FragmentLeftMenuAdapter(Context ctx) {
		robotoThin = Typeface.createFromAsset(ctx.getAssets(), "Roboto-Thin.ttf");
	}
	
	@Override
	public int getCount() {
		return menus.length;
	}

	@Override
	public String getItem(int position) {
		return menus[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null) {
			convertView = new TextView(parent.getContext());
		}
		TextView t = (TextView)convertView;
		t.setTextAppearance(parent.getContext(), R.style.Menuitem);
		
		t.setTypeface(robotoThin);
		int padding = (int)parent.getResources().getDimension(R.dimen.menu_item_padding);
		t.setPadding(padding,padding,padding,padding);
		t.setTag(drawables[position]);
		if(Integer.valueOf(drawables[position]).equals(selectedDrawable)) {
			t.setText(Html.fromHtml("<u>"+getItem(position)+"</u>"));
		} else {
			t.setText(getItem(position));
		}
		Drawable d = parent.getResources().getDrawable(drawables[position]);
		t.setCompoundDrawablesWithIntrinsicBounds(d,null, null, null);
		t.setCompoundDrawablePadding((int)parent.getResources().getDimension(R.dimen.menu_item_padding));
		return t;
	}

	public void setSelectedDrawable(Integer tag) {
		this.selectedDrawable = tag;
		notifyDataSetChanged();
	}

}
