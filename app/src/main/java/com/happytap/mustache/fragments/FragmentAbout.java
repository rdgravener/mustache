package com.happytap.mustache.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.flurry.android.FlurryAgent;
import com.happytap.mustache.R;

public class FragmentAbout extends Fragment {

	View ryan;
	View aidan;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_about, container,false);
		ryan = view.findViewById(R.id.ryan);
		aidan = view.findViewById(R.id.aidan);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		OnClickListener l = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				if(v==ryan) {
					FlurryAgent.logEvent("Ryan_Clicked");
					i.setData(Uri.parse("http://blog.ryangravener.com"));
				} else {
					FlurryAgent.logEvent("Aidan_Clicked");
					i.setData(Uri.parse("https://github.com/afeld/mustachio"));
				}
				startActivity(i);				
			}
		};
		ryan.setOnClickListener(l);
		aidan.setOnClickListener(l);
	}
	
}
