package com.happytap.mustache.fragments;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ShareCompat.IntentBuilder;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.ShareActionProvider.OnShareTargetSelectedListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.happytap.mustache.activity.MustacheActivity;
import com.happytap.mustache.R;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.Face;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.domain.PointF;
import com.happytap.mustache.domain.Purchases;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.ThreadHelper;

public class FragmentMustachePhoto extends Fragment {

	private View root;
	private ImageView image;

	private Photo _photo;
	private List<Face> _faces;
	private Bitmap mutable;
	private ViewPager pager;
	private TextView progressText;

	private int mustacheResource = R.raw.mustache1;
	private int pos = 1;

	Handler handler = new Handler();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = (ViewGroup) inflater.inflate(R.layout.fragment_show_mustache,
				container, false);
		image = (ImageView) root.findViewById(R.id.image);
		pager = (ViewPager) root.findViewById(R.id.pager);
		progressText = (TextView) root.findViewById(R.id.progress_text);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		ThreadHelper.getScheduler().submit(loadFacesFromDatabase);

	}
	
	public void setPhoto(Photo photo) {
		// TODO Auto-generated method stub
		_photo = photo;
	}

	private Runnable loadFacesFromDatabase = new Runnable() {
		public void run() {
			try {
				_faces = PhotoDao.getInstance().getFaces(_photo.id);
				
				if (_faces.isEmpty()) {
					return;
				}

				Bitmap photo = Images.loadImage(getActivity(), 1024, 1024,
						Uri.parse(_photo.fileName));
				mutable = photo.copy(Bitmap.Config.ARGB_8888, true);

				// float scale = photo.getWidth() / (float) _photo.resizedWidth;

				// for(Face f : _faces) {
				// System.out.println(f);
				//
				// PointF left = f.getMouthLeft();
				// int newLeftX = (int) (left.x*scale);
				// int newLeftY = (int) (left.y*scale);
				// PointF right = f.getMouthRight();
				// int newRightX = (int) (right.x*scale);
				// int newRightY = (int) (right.y*scale);
				// PointF nose = f.getNose();
				// int newNoseY = (int)(f.getNose().y*scale);
				// newLeftX*=.92;
				// newRightX*=1.08;
				// int width = newRightX - newLeftX;
				// float mustacheScale = (width / (float) mustache.getWidth());
				//
				// Bitmap newStache = Bitmap.createScaledBitmap(mustache, width,
				// (int)(mustacheScale*mustache.getHeight()), true);
				// int newY = newNoseY + (int)((newLeftY-newNoseY)*.25);
				// for(int i = 0; i < newStache.getWidth(); i++) {
				// for(int j = 0; j < newStache.getHeight(); j++) {
				// int pixel = newStache.getPixel(i,j);
				// if(Color.alpha(pixel)==0) {
				// continue;
				// }
				// int nx = newLeftX+i;
				// int ny = newY+j;
				// if(nx>0 && nx < mutable.getWidth() && ny>0 && ny <
				// mutable.getHeight()) {
				// mutable.setPixel(nx, ny, pixel);
				// }
				// }
				// }
				//
				//
				// }
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						image.setImageBitmap(mutable);
						handler.postDelayed(afterPhoto2, 500);
					};
				});
				getActivity().runOnUiThread(afterPhoto);
			} catch (Exception e) {
				e.printStackTrace();
			}

		};
	};

	private Runnable afterPhoto = new Runnable() {
		public void run() {
			getActivity().invalidateOptionsMenu();
		};
	};

	private Runnable afterPhoto2 = new Runnable() {
		public void run() {			 
			
			final List<Integer> original = new ArrayList<Integer>();
			for(int i = 0; i < 27; i++) {
				original.add(i);
			}
			
			//Random random = new Random();
			final List<Integer> ads = new ArrayList<Integer>(original);
			
			if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).contains(Purchases.REMOVE_ADS.getSku())) {
				int k = 0;
				for(int i = 0; i < 27; i++) {
					if(i%4==3) {
						//int next = random.nextInt(4);
						//if(next<2) {
							ads.add(i,null);
						//}
					}
				}
			}
			pager.setAdapter(new FragmentPagerAdapter(getActivity()
					.getSupportFragmentManager()) {
				public int getCount() {
					return ads.size();
				};

				@Override
				public Fragment getItem(int arg0) {
					Fragment f = null;
					if(ads.get(arg0)==null) {
						Bundle b= new Bundle();
						f = new FragmentAd();
						f.setArguments(b);
					} else {
						f = new FragmentMustacheOverlay();
						Bundle b = new Bundle();
						b.putSerializable("faces", (Serializable) _faces);
						b.putInt("width", image.getWidth());
						b.putInt("height", image.getHeight());
						b.putInt("photoWidth", mutable.getWidth());
						b.putInt("photoHeight", mutable.getHeight());
						b.putInt("position", ads.get(arg0));
						b.putSerializable("photo", _photo);
						f.setArguments(b);
					}
					return f;
				}
			});
			pager.setOffscreenPageLimit(2);
			pager.setOnPageChangeListener(new OnPageChangeListener() {
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageSelected(int pos) {
					MustacheActivity a = (MustacheActivity) getActivity();
					mustacheResource = getResources().getIdentifier(
							"mustache" + (pos + 1), "raw",
							a.getPackageName());
					FragmentMustachePhoto.this.pos = pos+1;
				}
			});
		};

	};

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(
				R.menu.mustache_photo_menu, menu);

		MenuItem item = menu.findItem(R.id.share);
		Intent i = new Intent(Intent.ACTION_SENDTO);
		IntentBuilder b = IntentBuilder.from(getActivity());
		b.setType("image/jpeg");
		b.setStream(Uri.parse(_photo.fileName));
		b.setSubject("http://wmwm.us/mustache");
		ShareActionProvider p = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
		p.setShareIntent(b.getIntent());
		p.setOnShareTargetSelectedListener(new OnShareTargetSelectedListener() {

			@Override
			public boolean onShareTargetSelected(ShareActionProvider source,
					final Intent intent) {
				// TODO Auto-generated method stub
				ThreadHelper.getScheduler().submit(new Runnable() {
					@Override
					public void run() {
						handler.post(generatingPhoto);
						try {
							float scale = mutable.getWidth()
									/ (float) _photo.resizedWidth;
							Bitmap mustache = BitmapFactory
									.decodeStream(getResources()
											.openRawResource(mustacheResource));
							List<int[][]> undos = new ArrayList<int[][]>();
							for (Face f : _faces) {

								PointF left = f.getMouthLeft();
								int newLeftX = (int) (left.x*scale);
								int newLeftY = (int) (left.y*scale);
								PointF right = f.getMouthRight();
								int newRightX = (int) (right.x*scale);
								int newRightY = (int) (right.y*scale);
								PointF nose = f.getNose();
								int newNoseY = (int)(f.getNose().y*scale);
								
								float deltaY = newRightY-newLeftY;
								float deltaX = newRightX - newLeftX;
								
								float angle = (float) (Math.atan2(deltaY, deltaX) * 180 / Math.PI);
								Matrix matrix = new Matrix();
								matrix.setRotate(angle,0,0);
								
								android.graphics.PointF k = new android.graphics.PointF(newLeftX,newLeftY);
								double hypotenuse = Math.sqrt(Math.pow(newRightX-newLeftX, 2)+Math.pow(newRightX-newLeftY, 2));
								double noseOffset = Math.sin(Math.toRadians(Math.abs(angle)))*hypotenuse;
								
								int resourceId = getResources().getIdentifier("mustache"+pos+"_width_scale", "integer", getActivity().getPackageName());
								float xScale = (getResources().getInteger(resourceId)/100f)/2f;
								newLeftX*=(1-xScale);
								newRightX*=(1+xScale);
								int width = newRightX - newLeftX;			
								float mustacheScale = (width / (float) mustache.getWidth());
								Bitmap rotated = Bitmap.createBitmap(mustache,0,0,mustache.getWidth(),mustache.getHeight(),matrix,true);
								final Bitmap newStache = Bitmap.createScaledBitmap(rotated, width, (int)(mustacheScale*rotated.getHeight()), true);
								newNoseY-=noseOffset;
								resourceId = getResources().getIdentifier("mustache"+pos+"_y_offset", "integer", getActivity().getPackageName());
								final int newY = newNoseY + (int)((newLeftY-newNoseY)*getResources().getInteger(resourceId)/100f);
								final int nnL = newLeftX;								
								int[][] undo = new int[newStache.getHeight()][newStache.getWidth()];
								undos.add(undo);
								for (int i = 0; i < newStache.getWidth(); i++) {
									for (int j = 0; j < newStache.getHeight(); j++) {
										int nx = nnL + i;
										int ny = newY + j;
										int pixel = newStache.getPixel(i, j);
										if (Color.alpha(pixel) == 0) {
											continue;
										}										
										if (nx > 0 && nx < mutable.getWidth()
												&& ny > 0
												&& ny < mutable.getHeight()) {
											undo[j][i] = mutable.getPixel(nx, ny);
											mutable.setPixel(nx, ny, pixel);
										}
										
									}
								}

							}
							Uri uri = Uri.parse(_photo.fileName);
							File file = new File(Images
									.getAlbumDir(getActivity()), uri
									.getLastPathSegment() + "_share.jpg");
							try {
								int[][] revert = null;
								if (mutable.getWidth() > 500
										&& mutable.getHeight() > 500) {

									Bitmap ad = BitmapFactory
											.decodeStream(getResources()
													.openRawResource(
															R.raw.ad_overlay));
									revert = new int[ad.getHeight()][ad
											.getWidth()];
									for (int i = 0; i < ad.getHeight(); i++) {
										for (int j = 0; j < ad.getWidth(); j++) {
											revert[i][j] = mutable.getPixel(
													j,
													mutable.getHeight()
															- ad.getHeight()
															+ i);
											mutable.setPixel(
													j,
													mutable.getHeight()
															- ad.getHeight()
															+ i,
													ad.getPixel(j, i));
										}
									}
								}
								mutable.compress(CompressFormat.JPEG, 100,
										new FileOutputStream(file));
								if (revert != null) {
									for (int i = 0; i < revert.length; i++) {
										for (int j = 0; j < revert[i].length; j++) {
											int newY = mutable.getHeight()
													- revert.length + i;
											mutable.setPixel(j, newY,
													revert[i][j]);
										}
									}
								}
								Iterator<int[][]> iter = undos.iterator();
								for (Face f : _faces) {

									PointF left = f.getMouthLeft();
									int newLeftX = (int) (left.x * scale);
									int newLeftY = (int) (left.y * scale);
									PointF right = f.getMouthRight();
									int newRightX = (int) (right.x * scale);
									int newRightY = (int) (right.y * scale);
									PointF nose = f.getNose();
									int newNoseY = (int) (f.getNose().y * scale);
									int resourceId = getResources().getIdentifier("mustache"+pos+"_width_scale", "integer", getActivity().getPackageName());
									float xScale = (getResources().getInteger(resourceId)/100f)/2f;
									newLeftX*=(1-xScale);
									newRightX*=(1+xScale);
									int width = newRightX - newLeftX;
									float mustacheScale = (width / (float) mustache
											.getWidth());
									int height = (int) (mustacheScale * mustache
											.getHeight());
									resourceId = getResources().getIdentifier("mustache"+pos+"_y_offset", "integer", getActivity().getPackageName());
									final int newY = newNoseY + (int)((newLeftY-newNoseY)*getResources().getInteger(resourceId)/100f);
									int[][] undo = iter.next();
									for (int i = 0; i < undo.length; i++) {
										for (int j = 0; j < undo[i].length; j++) {
											int nx = newLeftX + j;
											int ny = newY + i;
											int pixel = undo[i][j];
											if (Color.alpha(pixel) == 0) {
												continue;
											}	
											if (nx > 0 && nx < mutable.getWidth()
													&& ny > 0
													&& ny < mutable.getHeight()) {
												mutable.setPixel(nx, ny, pixel);
											}
											
										}
									}
								}
								intent.putExtra(Intent.EXTRA_STREAM,
										Uri.fromFile(file));
								startActivity(intent);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						finally {
							handler.removeCallbacks(generatingPhoto);
							handler.post(hideGenerating);
						}
					}
				});
				return true;

			}

		});
	}

	private Runnable hideGenerating = new Runnable() {
		public void run() {
			progressText.setVisibility(View.GONE);
		};
	};

	private Runnable generatingPhoto = new Runnable() {
		private int dots = 0;

		public void run() {
			String d = "";
			for (int i = 0; i < dots; i++) {
				d += ".";
			}
			if (progressText.getVisibility() != View.VISIBLE) {
				progressText.setVisibility(View.VISIBLE);
			}
			progressText.setText("Mustachifying photo" + d);
			dots++;
			if (dots > 3) {
				dots = 0;
			}
			//handler.postDelayed(generatingPhoto, 600);
		};
	};
}
