package com.happytap.mustache.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.interfaces.OnPhotoPicked;

public class MustacheFragmentPagerAdapter extends FragmentPagerAdapter {

	private State state = State.PICK_PHOTO;
	
	private Photo photo;
	
	public static enum State {
		PICK_PHOTO, PHOTO_PICKED;
	}
	
	private OnPhotoPicked onPhotoPicked = new OnPhotoPicked() {
		@Override
		public void onPhoto(Photo photo) {
			MustacheFragmentPagerAdapter.this.photo = photo;
			state = State.PHOTO_PICKED;
			destroyItem(null, 0, null);
			notifyDataSetChanged();
			
		}
	};
	
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	};
	
	public MustacheFragmentPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int pos) {
		Fragment fragment = null;
		if(state==State.PICK_PHOTO) {
			FragmentPickPhoto p = new FragmentPickPhoto();
			p.setOnPhotoPicked(onPhotoPicked);
			fragment = p;
		} else
		if(state==State.PHOTO_PICKED) {
			FragmentShowPhoto p = new FragmentShowPhoto();
			if(photo==null) {
				photo = PhotoDao.getInstance().getLatest();
			}
			Bundle b = new Bundle();
			b.putString("fileName", photo.fileName);
			p.setArguments(b);
			fragment = p;
		}
		return fragment;
	}

	@Override
	public int getCount() {
		return 1;
	}

}
