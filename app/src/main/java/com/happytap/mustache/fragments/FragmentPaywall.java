//package com.happytap.mustache.fragments;
//
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.Future;
//
//import org.json.JSONObject;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.PackageManager.NameNotFoundException;
//import android.os.Bundle;
//import android.os.Handler;
//import android.preference.PreferenceManager;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.BaseAdapter;
//import android.widget.ListView;
//
//import com.flurry.android.FlurryAgent;
//import com.happytap.mustache.R;
//import com.happytap.mustache.iab.IabException;
//import com.happytap.mustache.iab.IabHelper;
//import com.happytap.mustache.iab.IabHelper.OnIabPurchaseFinishedListener;
//import com.happytap.mustache.iab.IabHelper.OnIabSetupFinishedListener;
//import com.happytap.mustache.iab.IabResult;
//import com.happytap.mustache.iab.Inventory;
//import com.happytap.mustache.iab.Purchase;
//import com.happytap.mustache.iab.SkuDetails;
//import com.happytap.mustache.util.ThreadHelper;
//import com.happytap.mustache.views.ListItemPrice;
//
//public class FragmentPaywall extends Fragment {
//
//	private IabHelper helper;
//
//	private static final String SIGNATURE = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlWG7d4GfdvfzolBVhnWSOhgaOPnJx2Un6EZC4KXsdwflOmx/Gum+u09p6ujli7Mn8CcIe3WXvDtOo/vVKOzZncpDcq2TqOxogdE6JyDF94CA6Sqxmj9KniPMooo5wuYSyEg70U5zUtoNN1Rc1tQtwtsWJERRRCIiaKHN3a3I4Sv3vxBEZTJZuj5KM5BOf748hbcU6lQcnCwdpmO1z4VO6SLXz14V6S/hZ3RFhxlFiJeFedSrKJGFc5BdiXREF7NoIvjDG7ZB8XvkG4sz9vBvMZdCVglL+KJRaoKzn5khhoN50Aq9uE3I0Yz2mAbrps6sTJoYmdQI6QzgsiDILDngYwIDAQAB";
//
//	private Future<?> queryInventoryTask;
//
//	private View root;
//
//	private ListView list;
//
//	private FragmentPaywallAdapter adapter;
//
//	private Inventory inventory;
//
//	private static final String[] SKUS = new String[]{"remove.ads","remove.branding"};
//	private static final String[] NAMES = new String[]{"Remove Advertisements", "Remove Branding"};
//
//	private Handler handler = new Handler();
//
//	private static int COUNT = 0;
//	private static long CREATED = System.currentTimeMillis();
//
//	private SharedPreferences prefs;
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		root = inflater.inflate(R.layout.fragment_paywall, container,false);
//		list = (ListView) root.findViewById(R.id.list);
//		return root;
//	}
//
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onActivityCreated(savedInstanceState);
//		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
//		COUNT++;
//		PackageInfo info;
//		try {
//			info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
//		} catch (NameNotFoundException e) {
//			throw new RuntimeException(e);
//		}
//
//		helper = new IabHelper(getActivity(), SIGNATURE) {
//
//		};
//		helper.startSetup(new OnIabSetupFinishedListener() {
//
//			@Override
//			public void onIabSetupFinished(IabResult result) {
//				if(!result.isSuccess()) {
//					//TODO: THIS NEEDS TO BE DONE
//				} else {
//					ThreadHelper.getScheduler().submit(queryInventory);
//				}
//			}
//		});
//
//		list.setAdapter(adapter = new FragmentPaywallAdapter());
//		list.setOnItemClickListener(onItemClickListener);
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if(requestCode==1) {
//			FlurryAgent.endTimedEvent("Purcase_Sku");
//			helper.handleActivityResult(requestCode, resultCode, data);
//			adapter.notifyDataSetChanged();
//		}
//	}
//
//	private OnItemClickListener onItemClickListener = new OnItemClickListener() {
//		@Override
//		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//				long arg3) {
//			String sku = arg1.getTag().toString();
//			if(inventory!=null && !inventory.hasPurchase(sku)) {
//				JSONObject j = new JSONObject();
//				try {
//					j.put("created", CREATED);
//					j.put("count", COUNT);
//					j.put("now", System.currentTimeMillis());
//				}catch (Exception e) {
//
//				}
//				Map<String, String> args = new HashMap<String,String>();
//				args.put("Sku",sku);
//				args.put("Created", String.valueOf(CREATED));
//				args.put("Count", String.valueOf(COUNT));
//				FlurryAgent.logEvent("Purchase_Sku", args, true);
//				helper.launchPurchaseFlow(getActivity(), sku, 1, onIabPurchaseFinishedListener,j.toString());
//			}
//		}
//	};
//
//	public void setOnIabPurchaseFinishedListener(
//			OnIabPurchaseFinishedListener onIabPurchaseFinishedListener) {
//		this.listener = onIabPurchaseFinishedListener;
//	}
//
//	private OnIabPurchaseFinishedListener listener;
//
//	private OnIabPurchaseFinishedListener onIabPurchaseFinishedListener = new OnIabPurchaseFinishedListener() {
//
//		@Override
//		public void onIabPurchaseFinished(IabResult result, Purchase info) {
//			if(result.isSuccess()) {
//				prefs.edit().putString(info.getSku(), info.getToken()).commit();
//				listener.onIabPurchaseFinished(result, info);
//				adapter.notifyDataSetChanged();
//
//			}
//		}
//	};
//
//	private class FragmentPaywallAdapter extends BaseAdapter {
//		@Override
//		public int getCount() {
//			return SKUS.length;
//		}
//
//		@Override
//		public Object getItem(int position) {
//			return null;
//		}
//
//		@Override
//		public long getItemId(int position) {
//			return 0;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			ListItemPrice tv = new ListItemPrice(parent.getContext());
//			String sku = SKUS[position];
//			String name = NAMES[position];
//			tv.setTag(sku);
//			if(inventory!=null) {
//				SkuDetails details = inventory.getSkuDetails(sku);
//				tv.setSkuDetails(name, details,inventory.getPurchase(sku)!=null || prefs.contains(sku));
//			} else {
//				tv.setSkuDetails(name, null,false);
//			}
//
//			return tv;
//		}
//	};
//
//	private Runnable queryInventory = new Runnable() {
//		public void run() {
//			try {
//				inventory = helper.queryInventory(true, Arrays.asList(SKUS));
//
//				handler.post(new Runnable() {
//					public void run() {
//						adapter.notifyDataSetChanged();
//					};
//				});
//			} catch (IabException e) {
//				e.printStackTrace();
//			}
//		};
//	};
//
//}
