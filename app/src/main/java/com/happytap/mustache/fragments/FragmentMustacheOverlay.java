package com.happytap.mustache.fragments;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.happytap.mustache.R;
import com.happytap.mustache.domain.Face;
import com.happytap.mustache.domain.Photo;
import com.happytap.mustache.domain.PointF;
import com.happytap.mustache.util.ThreadHelper;

public class FragmentMustacheOverlay extends Fragment {
	
	RelativeLayout root;
	
	RelativeLayout centered;
	
	Handler handler = new Handler();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = (RelativeLayout)inflater.inflate(R.layout.fragment_mustache_overlay, null);
		return root;
	}
	
	private Runnable runnable = new Runnable() {
		public void run() {
			Bundle args = getArguments();
			
			List<Face> faces = (List<Face>)args.getSerializable("faces");
			Photo _photo = (Photo)args.getSerializable("photo");
			System.out.println(args.getInt("width"));
			System.out.println(args.getInt("height"));
			
			int imageWidth = args.getInt("width");
			final int imageHeight = args.getInt("height");
			int photoWidth = args.getInt("photoWidth");
			int photoHeight = args.getInt("photoHeight");
			
			int position = (args.getInt("position") + 1);
			
			int resId = getResources().getIdentifier("mustache"+position, "raw", getActivity().getPackageName());
			
			Bitmap mustache = BitmapFactory.decodeStream(getResources().openRawResource(resId));
			float scale =  imageWidth / (float) _photo.resizedWidth;					
			
			handler.post(new Runnable() {
				public void run() {
					LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, imageHeight);
					lp.addRule(RelativeLayout.CENTER_VERTICAL);
					centered = new RelativeLayout(getActivity());
					root.addView(centered,lp);
				};
			});
			
			
			for(Face f : faces) {
				System.out.println(f);
				
				PointF left = f.getMouthLeft();
				int newLeftX = (int) (left.x*scale);
				int newLeftY = (int) (left.y*scale);
				PointF right = f.getMouthRight();
				int newRightX = (int) (right.x*scale);
				int newRightY = (int) (right.y*scale);
				PointF nose = f.getNose();
				int newNoseY = (int)(f.getNose().y*scale);
				
				float deltaY = newRightY-newLeftY;
				float deltaX = newRightX - newLeftX;
				
				float angle = (float) (Math.atan2(deltaY, deltaX) * 180 / Math.PI);
				
				System.out.println(angle);
				
				Matrix matrix = new Matrix();
				matrix.setRotate(angle,0,0);
				
				android.graphics.PointF k = new android.graphics.PointF(newLeftX,newLeftY);
				double hypotenuse = Math.sqrt(Math.pow(newRightX-newLeftX, 2)+Math.pow(newRightX-newLeftY, 2));
				double noseOffset = Math.sin(Math.toRadians(Math.abs(angle)))*hypotenuse;
				int resourceId = getResources().getIdentifier("mustache"+position+"_width_scale", "integer", getActivity().getPackageName());
				float xScale = (getResources().getInteger(resourceId)/100f)/2f;
				newLeftX*=(1-xScale);
				newRightX*=(1+xScale);
				int width = newRightX - newLeftX;			
				float mustacheScale = (width / (float) mustache.getWidth());
				int max = (int)Math.hypot((double)mustache.getWidth(), (double)mustache.getHeight());
				//int diffWidth =  tmp.getWidth()-mustache.getWidth();
				//int diffHeight = tmp.getHeight()-mustache.getHeight();
//				int offsetX = 0;
//				int offsetY = 0;
//				if(diffWidth>0) {
//					offsetX = diffWidth/2;
//				} else {
//					offsetY = diffHeight/2;
//				}
//				for(int i = 0; i < mustache.getWidth(); i++) {
//					for(int j = 0; j < mustache.getHeight(); j++) {
//						tmp.setPixel(offsetX+i, offsetY+j, mustache.getPixel(i, j));
//					}
//				}
				Bitmap rotated = Bitmap.createBitmap(mustache,0,0,mustache.getWidth(),mustache.getHeight(),matrix,true);
				final Bitmap newStache = Bitmap.createScaledBitmap(rotated, width, (int)(mustacheScale*rotated.getHeight()), true);
				System.out.println(newStache.getWidth()+"x"+newStache.getHeight());
				newNoseY-=noseOffset;
				resourceId = getResources().getIdentifier("mustache"+position+"_y_offset", "integer", getActivity().getPackageName());
				final int newY = newNoseY + (int)((newLeftY-newNoseY)*getResources().getInteger(resourceId)/100f);
				final int nnL = newLeftX;
				handler.post(new Runnable() {
					public void run() {						
						ImageView i = new ImageView(getActivity());
						//i.setBackgroundColor(0x55220000);
						LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, imageHeight);
						i.setImageBitmap(newStache);
						lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						lp.leftMargin = nnL;
						lp.topMargin = newY;
						
						centered.addView(i,lp);
					};
				});
				
			}
		};
	};
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);		
		//root.addView(centered,lp);
		ThreadHelper.getScheduler().submit(runnable);		
	}
	
	
}
