package com.happytap.mustache.fragments;

import java.io.File;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.concurrent.Future;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.happytap.mustache.R;
import com.happytap.mustache.database.DBHelper;
import com.happytap.mustache.database.PhotoDao;
import com.happytap.mustache.util.Images;
import com.happytap.mustache.util.ThreadHelper;
import com.happytap.mustache.views.MenuItemView;

public class HistoryAdapter extends CursorAdapter {

	private static final DateFormat FORMATTER = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
	
	private Handler handler = new Handler();
	
	public HistoryAdapter(Context ctx) {
		super(ctx, null, false);
		changeCursor(DBHelper.getInstance().getReadableDatabase()
				.rawQuery("select id as _id, file_name, created, thumbnail from photos order by created desc", null));
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void bindView(View v, final Context arg1, Cursor c) {
		// TODO Auto-generated method stub
		MenuItemView view = (MenuItemView) v;
		final String fileName = getFileName(c);
		String created = getCreated(c);
		String id = getId(c);
		String thumb = getThumbnail(c);
		view.update(id, fileName, thumb, created);
		if(thumb==null) {
			final ViewHolder h = (ViewHolder)view.getTag();
			if(h.thumb!=null) {
				h.thumb.cancel(true);
			}
			h.id = id;
			h.fileName = fileName;
			h.thumb = ThreadHelper.getScheduler().submit(new Runnable() {
				@Override
				public void run() {
					try {
					Context c = arg1;
					int dimen = (int) arg1.getResources().getDimension(R.dimen.thumbnailSize);						
					File file = Images.resizeImage(c, dimen, dimen, 90, "thumb", Uri.parse(h.fileName));
					BitmapFactory.Options o = Images.getImageSize(c, Uri.fromFile(file));
					PhotoDao.getInstance().updateThumbnail(h.id, Uri.fromFile(file).toString(), o.outWidth, o.outHeight);
					handler.post(new Runnable() {
						@Override
						public void run() {
							notifyDataSetChanged();
						}
					});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	
	private String getId(Cursor c) {
		return c.getString(0);
	}
	
	private String getFileName(Cursor c) {
		return c.getString(1);			
	}
	
	private String getCreated(Cursor c) {
		long created = c.getLong(2);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(created);
		return FORMATTER.format(cal.getTime());			
	}
	
	private String getThumbnail(Cursor c) {
		return c.getString(3);
	}

	@Override
	public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
		View v =  new MenuItemView(arg0);
		ViewHolder h = new ViewHolder();			
		v.setTag(h);
		return v;
	}
	
	private static class ViewHolder {
		Future<?> thumb;
		String fileName;
		String id;
	}

}
