package com.happytap.mustache.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.happytap.mustache.service.PhotoService;

/**
 * Created by gravener on 9/13/14.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            context.startService(new Intent(context, PhotoService.class));
        }
    }
}
