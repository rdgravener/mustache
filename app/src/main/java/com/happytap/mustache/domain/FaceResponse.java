package com.happytap.mustache.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class FaceResponse {

	public String url;
	
	private List<Face> faces;
	
	private boolean success;
	
	private int quota;		
	
	public FaceResponse() {
		
	}
	
	public FaceResponse(JSONObject o) {
		url = o.optString("url");
		if(o.optJSONObject("usage")!=null) {
			JSONObject usage = o.optJSONObject("usage");
			success = "Succeed.".equalsIgnoreCase(usage.optString("status"));
			quota = usage.optInt("quota");
		}
		JSONArray fd = o.optJSONArray("face_detection");
		if(fd!=null) {
			faces = new ArrayList<Face>(fd.length());
			for(int i = 0; i < fd.length(); i++) {
				JSONObject ob = fd.optJSONObject(i);
				Face f = new Face(ob);
				faces.add(f);
			}
		} else {
			faces = Collections.emptyList();
		}
	}
	
	public int getQuota() {
		return quota;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public List<Face> getFaces() {
		if(faces==null) {
			faces = Collections.emptyList();
		}
		return faces;
	}

    public static FaceResponse newFaceResponse(List<FaceSimple> f) {
        FaceResponse r = new FaceResponse();
        List<Face> faces = new ArrayList<Face>(f.size());
        for(FaceSimple fs : f) {
            faces.add(Face.toFace(fs));
        }
        r.faces = faces;
        r.success = true;
        return r;
    }
	
}

