package com.happytap.mustache.domain;

import java.io.Serializable;

public class Photo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String fileName;
	public String resizedFileName;
	public String thumbnail;
	public int thumbnailWidth;
	public int thumbnailHeight;
	public String url;
	public long created;
	
	public String hash;
	
	public String id;
	
	public int width;
	public int height;
	
	public int resizedHeight;
	public int resizedWidth;
	
	public boolean facesChecked;

}
