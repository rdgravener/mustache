package com.happytap.mustache.domain;

public enum Purchases {

	REMOVE_ADS("remove.ads","Remove Advertisements"), REMOVE_BRANDING("remove.branding", "Remove Branding");
	
	private Purchases(String sku, String name) {
		this.sku = sku;
		this.name = name;
	}
	
	String sku;
	
	public String getSku() {
		return sku;
	}
	
	String name;
	
}
