package com.happytap.mustache.domain;

import java.io.File;

/**
 * Created by gravener on 9/13/14.
 */
public class Media {
    private File file;
    @SuppressWarnings("unused")
    private String type;

    public Media(File file, String type) {
        this.file = file;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public File getFile() {
        return file;
    }
}
