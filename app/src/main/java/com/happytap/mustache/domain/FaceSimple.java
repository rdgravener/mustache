package com.happytap.mustache.domain;


import java.lang.reflect.Field;

/**
 * Created by gravener on 9/13/14.
 */
public class FaceSimple {

    PointF eyeLeftLeft;
    PointF eyeLeftRight;
    PointF eyeRightLeft;
    PointF eyeRightRight;
    PointF nose;
    PointF mouthLeft;
    PointF mouthRight;

    public FaceSimple setEyeLeftLeft(PointF point) {
        this.eyeLeftLeft = point;
        return this;
    }

    public FaceSimple setEyeLeftRight(PointF point) {
        this.eyeLeftRight = point;
        return this;
    }

    public FaceSimple setEyeRightLeft(PointF point) {
        this.eyeRightLeft = point;
        return this;
    }

    public FaceSimple setEyeRightRight(PointF point) {
        this.eyeRightRight = point;
        return this;
    }

    public FaceSimple setNose(PointF point) {
        this.nose = point;
        return this;
    }

    public FaceSimple setMouthLeft(PointF point) {
        this.mouthLeft = point;
        return this;
    }

    public FaceSimple setMouthRight(PointF point) {
        this.mouthRight = point;
        return this;
    }

    public PointF getEyeLeftLeft() {
        return eyeLeftLeft;
    }

    public PointF getEyeLeftRight() {
        return eyeLeftRight;
    }

    public PointF getEyeRightLeft() {
        return eyeRightLeft;
    }

    public PointF getEyeRightRight() {
        return eyeRightRight;
    }

    public PointF getNose() {
        return nose;
    }

    public PointF getMouthLeft() {
        return mouthLeft;
    }

    public PointF getMouthRight() {
        return mouthRight;
    }

    @Override
    public String toString() {
        return getMouthLeft().x+","+getMouthLeft().y + " to " + getMouthRight().x + "," + getMouthRight().y;
    }

    public void recalibrateFace(float percentage) {
        Field[] fields = getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                PointF pf = (PointF) field.get(this);
                if(pf==null) {
                    continue;
                }
                pf.x = pf.x*percentage;
                pf.y = pf.y*percentage;
            }
        } catch (Exception e) {

        }
    }
}
