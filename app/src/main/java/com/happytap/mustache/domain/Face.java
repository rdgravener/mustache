package com.happytap.mustache.domain;

import java.io.Serializable;

import org.json.JSONObject;


public class Face implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public PointF eyeLeft;
	public PointF eyeRight;
	
	public PointF mouthLeft;
	public PointF mouthRight;
	
	public PointF nose;
	
	public float age;
	public float smile;
	public float glasses;
	public float eyeClosed;
	public float mouthOpenWide;
	public float sex;
	public float confidence;
	
	public Face() {}
	
	public Face(JSONObject o) {
		JSONObject eye = o.optJSONObject("eye_left");
		if(eye!=null) {
			eyeLeft = new PointF((float)eye.optDouble("x"),(float)eye.optDouble("y"));
		}
		eye = o.optJSONObject("eye_right");
		if(eye!=null) {
			eyeRight = new PointF((float)eye.optDouble("x"),(float)eye.optDouble("y"));
		}
		JSONObject mouth = o.optJSONObject("mouth l");
		if(mouth!=null) {
			mouthLeft = new PointF((float)mouth.optDouble("x"), (float) mouth.optDouble("y"));
		}
		mouth = o.optJSONObject("mouth r");
		if(mouth!=null) {
			mouthRight = new PointF((float)mouth.optDouble("x"), (float) mouth.optDouble("y"));
		}
		JSONObject nose = o.optJSONObject("nose");
		if(nose!=null) {
			this.nose = new PointF((float)nose.optDouble("x"), (float) nose.optDouble("y"));
		}
		age = o.optInt("age");
		smile = o.optInt("smile");
		glasses = o.optInt("glasses");
		eyeClosed = o.optInt("eye_closed");
		mouthOpenWide = o.optInt("mouth_open_wide");
		sex = o.optInt("sex");
		confidence = o.optInt("confidence");
	}
	
	public PointF getEyeLeft() {
		return eyeLeft;
	}
	
	public PointF getEyeRight() {
		return eyeRight;
	}
	
	public PointF getMouthLeft() {
		return mouthLeft;
	}
	
	public PointF getMouthRight() {
		return mouthRight;
	}
	
	public PointF getNose() {
		return nose;
	}

	@Override
	public String toString() {
		return "Face [eyeLeft=" + eyeLeft + ", eyeRight=" + eyeRight
				+ ", mouthLeft=" + mouthLeft + ", mouthRight=" + mouthRight
				+ ", nose=" + nose + ", age=" + age + ", smile=" + smile
				+ ", glasses=" + glasses + ", eyeClosed=" + eyeClosed
				+ ", mouthOpenWide=" + mouthOpenWide + ", sex=" + sex
				+ ", confidence=" + confidence + "]";
	}
	
	public static Face toFace(FaceSimple s) {
        Face face = new Face();
        face.mouthLeft = s.mouthLeft;
        face.mouthRight = s.mouthRight;
        face.nose = s.nose;
        return face;
    }
	
}
