package com.happytap.mustache.domain;

import java.io.Serializable;

public class PointF implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public float x;
	public float y;
	
	public PointF(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	
}
