package com.example.mymodule.app2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ScaleGestureDetectorCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends Activity {

    ImageView selectedImage;

    ViewGroup root;

    Map<ImageView,ImageOptions> options = new HashMap<ImageView,ImageOptions>();

    ScaleGestureDetector.OnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            System.out.println("onScale " + detector.getScaleFactor());
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
            lp.width = (int) (beginWidth * detector.getScaleFactor());
            lp.height = (int) (detector.getScaleFactor()*beginHeight);
            selectedImage.setLayoutParams(lp);
            return false;
        }

        int beginWidth;
        int beginHeight;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            isScaling = true;
            System.out.println("onScaleBegin " + detector.getScaleFactor());
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
            beginWidth = lp.width;
            beginHeight = lp.height;
            return true;
        }

        Runnable deled = new Runnable() {
            @Override
            public void run() {
                isScaling = false;
            }
        };

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            System.out.println("onScaleEnd " + detector.getScaleFactor());
            handler.removeCallbacks(deled);
            handler.postDelayed(deled,150);

        }
    };

    RotateGestureDetector.OnRotateGestureListener rotateGestureListener = new RotateGestureDetector.OnRotateGestureListener() {

        float isFirst = 0;

        @Override
        public boolean onRotate(RotateGestureDetector detector) {
            ImageOptions option = options.get(selectedImage);
            if(option==null) {
                return false;
            }
            if(isFirst==0) {
                isFirst = option.rotate;
            }
            option.rotate=(isFirst-detector.getRotationDegreesDelta());
            selectedImage.invalidate();
            return false;
        }

        @Override
        public boolean onRotateBegin(RotateGestureDetector detector) {
            System.out.println("onRotateBegin " + -detector.getRotationDegreesDelta());
            //rotate-=detector.getRotationDegreesDelta();
            return true;
        }

        @Override
        public void onRotateEnd(RotateGestureDetector detector) {
            System.out.println("onRotateEnd");
            isFirst = 0;
        }
    };

    boolean isScaling = false;

    MoveGestureDetector.OnMoveGestureListener moveGestureListener = new MoveGestureDetector.OnMoveGestureListener() {

        int prevXDiff;
        int prevYDiff;

        int startX;
        int startY;

        @Override
        public boolean onMove(MoveGestureDetector detector) {
            if(isScaling || detector.mCurrEvent.getPointerCount()>1) {
                if(isScaling) {
                    ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
                    lp.leftMargin = startX;
                    lp.topMargin = startY;
                    selectedImage.setLayoutParams(lp);
                }
                return false;
            }
            PointF f = detector.getFocusDelta();
            System.out.println("onMove " +f.x + " " + f.y);
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
            lp.leftMargin+=(f.x-prevXDiff);
            lp.topMargin+=(f.y-prevYDiff);
            prevXDiff = (int)f.x;
            prevYDiff = (int)f.y;
            selectedImage.setLayoutParams(lp);
            return false;
        }

        @Override
        public boolean onMoveBegin(MoveGestureDetector detector) {
            System.out.println("onMoveBegin");
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
            startX = lp.leftMargin;
            startY = lp.topMargin;
            if(isScaling || detector.mCurrEvent.getPointerCount()>1) {
                return false;
            }
            return true;
        }

        @Override
        public void onMoveEnd(MoveGestureDetector detector) {
            System.out.println("onMoveEnd");
            if(isScaling) {
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) selectedImage.getLayoutParams();
                lp.leftMargin = startX;
                lp.topMargin = startY;
                selectedImage.setLayoutParams(lp);
            }
            startX = 0;
            startY = 0;
            prevXDiff = 0;
            prevYDiff = 0;
        }
    };


    ScaleGestureDetector detector;

    RotateGestureDetector rotateGestureDetector;

    MoveGestureDetector moveGestureDetector;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        detector = new ScaleGestureDetector(this,scaleGestureListener);
        ScaleGestureDetectorCompat.setQuickScaleEnabled(detector,true);
        rotateGestureDetector = new RotateGestureDetector(this,rotateGestureListener);
        moveGestureDetector = new MoveGestureDetector(this,moveGestureListener);
        root = (ViewGroup) findViewById(R.id.root);
        loadBackground();
        for(int i = 0; i < 6; i++) {
            ImageView image = new ImageView(this) {
                @Override
                protected void onDraw(Canvas canvas) {
                    ImageOptions option = options.get(this);
                    canvas.rotate(option.rotate, canvas.getWidth() / 2f, canvas.getHeight() / 2f);
                    super.onDraw(canvas);
                }
            };
            options.put(image,new ImageOptions());
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.mustache1);
            int max = Math.max(b.getWidth(),b.getHeight());
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(max,max);
            image.setLayoutParams(lp);
            image.setImageBitmap(b);

            root.addView(image,root.getChildCount());
        }

        root.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                System.out.println("onTouch");
                if(motionEvent.getActionMasked()==MotionEvent.ACTION_DOWN) {
                    ImageView closest = null;
                    float dist = Integer.MAX_VALUE;
                    Rect r = new Rect();
                    for(int i = 1; i < root.getChildCount(); i++) {
                        ImageView image = (ImageView) root.getChildAt(i);
                        image.getHitRect(r);
                        int xa = (int)motionEvent.getX();
                        int xb = r.centerX();
                        int ya = (int)motionEvent.getY();
                        int yb = r.centerY();
                        float dd = (float)(Math.sqrt(Math.pow(xa-xb,2) + Math.pow(ya-yb,2)));
                        if(dd<dist) {
                            dist = dd;
                            closest = image;
                        }
                    }
                    selectedImage = closest;
                }
                detector.onTouchEvent(motionEvent);
                rotateGestureDetector.onTouchEvent(motionEvent);
                moveGestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadBackground();
            }
        },1000);
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                Rect r = new Rect();
//                image.getHitRect(r);
//                r.left-=150;
//                r.right+=150;
//                r.top-=150;
//                r.bottom+=150;
//                TouchDelegate d = new TouchDelegate(r,image);
//                ViewGroup g = (ViewGroup) image.getParent();
//                g.setTouchDelegate(d);
//            }
//        });

    }

    private void loadBackground() {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(),R.drawable.obama,o);
        int targetWidth = Math.min(root.getMeasuredWidth(),2048);
        int targetHeight = Math.min(root.getMeasuredHeight(),2048);
        int in = calculateInSampleSize(o,targetWidth,targetHeight);
        o.inJustDecodeBounds = false;
        o.inSampleSize = in;
        Bitmap b = BitmapFactory.decodeResource(getResources(),R.drawable.obama,o);
        ImageView v = new ImageView(this);
        v.setImageBitmap(b);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(b.getWidth(),b.getHeight());
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        root.addView(v,0,lp);
        //root.setBackground(new BitmapDrawable(getResources(),b));
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    class ImageOptions {
        float rotate;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
